﻿using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class Intersection : Location
{
    //Adjacent edges;
    //Be sure to check for null, an intersection can have 2-3 adjacent edges.
    public static readonly int MAX_ADJ_EDGES = 3;
    private List<Edge> _edges = new List<Edge>( );
    public List<Edge> Edges
    {
        get
        {
            return _edges;
        }
        private set
        {
            _edges = value;
        }
    }

    //Adjacent hexes;
    //Be sure to check for null, an intersection can have 1-3 adjacent hexes.
    public static readonly int MAX_ADJ_HEXES = 3;
    private List<Hex> _hexes = new List<Hex>( );
    public List<Hex> Hexes
    {
        get
        {
            return _hexes;
        }
        private set
        {
            _hexes = value;
        }
    }

    //We can determine an edge's position on the board based on the coordinates
    //of the three hexes adjacent to it.
    //These are used on clients to find adjacent networked edges/hexes.
    [SyncVar]
    public int _firstHexX;

    [SyncVar]
    public int _firstHexY;

    [SyncVar]
    public int _secondHexX;

    [SyncVar]
    public int _secondHexY;

    [SyncVar]
    public int _thirdHexX;

    [SyncVar]
    public int _thirdHexY;

    protected void Start( )
    {
        if( !isServer )
        {
            MakeAssociations( );
        }

        transform.parent = GameObject.Find( "Board/Intersections" ).transform;
    }

    //This method is meant to be called by non-server clients to create the
    //necessary associations between all of the networked board locations.
    [Client]
    protected override void MakeAssociations( )
    {
        BoardController controller = Controller;
        if( controller != null )
        {
            controller.RegisterLocation( this );
        }
        //TODO (eric): How to figure out if firstHex -> secondHex -> thirdHex is clockwise or counterclockwise around the intersection?

        //Get the direction from the first Hex to the second Hex
        //The index of this direction is the direction of this intersection
        //relative to the first hex.
        Tuple<int, int> firstToSecond = new Tuple<int, int>( _secondHexX - _firstHexX
                                                           , _secondHexY - _firstHexY );
        int firstHexDirectionIndex = Hex.GetDirectionIndex( firstToSecond );

        //Get the direction from the second Hex to the third Hex
        //The index of this direction is the direction of this intersection
        //relative to the second hex.
        Tuple<int, int> secondToThird = new Tuple<int, int>( _thirdHexX - _secondHexX
                                                           , _thirdHexY - _secondHexY );
        int secondHexDirectionIndex = Hex.GetDirectionIndex( secondToThird );

        //Get the direction from the second Hex to the third Hex
        //The index of this direction is the direction of this intersection
        //relative to the second hex.
        Tuple<int, int> thirdToFirst = new Tuple<int, int>( _firstHexX - _thirdHexX
                                                          , _firstHexY - _thirdHexY );
        int thirdHexDirectionIndex = Hex.GetDirectionIndex( thirdToFirst );

        //With these first two directions, we can determine whether the path
        //from firstHex -> secondHex -> thirdHex moves clockwise or
        //counterclockwise around this intersection.

        //If the direction is CW, then the directionIndex of
        //secondHex -> thirdHex will be 2 more than the directionIndex of
        //firstHex -> secondHex.

        //This information is necessary to we can attach this intersection to
        //the proper direction index on the Hex.
        bool hexesAreCw = false;

        if( secondHexDirectionIndex == ( (firstHexDirectionIndex + 2) % 6 ) )
        {
            hexesAreCw = true;
        }

        //Attempt to find the first adjacent hex
        Hex firstHex = BoardController.FindHexInScene( _firstHexX, _firstHexY );

        if( firstHex != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this intersection and it.
            if( hexesAreCw )
            {
                firstHex.AttachIntersection( this, firstHexDirectionIndex );
            }
            else
            {
                firstHex.AttachIntersection( this, Hex.GetCcwDirectionIndex( firstHexDirectionIndex ) );
            }
            AttachHex( firstHex );
        }

        //Attempt to find the edge between the first and second hexes
        Edge firstEdge = BoardController.FindEdgeInScene( _firstHexX, _firstHexY
                                                        , _secondHexX, _secondHexY );

        if( firstEdge != null )
        {
            //We found an already created network edge in the scene.
            //Create the association between this intersection and it.
            firstEdge.AttachIntersection( this );
            AttachEdge( firstEdge );
        }

        //Attempt to find the second adjacent hex
        Hex secondHex = BoardController.FindHexInScene( _secondHexX, _secondHexY );

        if( secondHex != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this intersection and it.
            if( hexesAreCw )
            {
                secondHex.AttachIntersection( this, secondHexDirectionIndex );
            }
            else
            {
                secondHex.AttachIntersection( this, Hex.GetCcwDirectionIndex( secondHexDirectionIndex ) );
            }
            AttachHex( secondHex );
        }

        //Attempt to find the edge between the second and third hexes
        Edge secondEdge = BoardController.FindEdgeInScene( _secondHexX, _secondHexY
                                                         , _thirdHexX, _thirdHexY );

        if( secondEdge != null )
        {
            //We found an already created network edge in the scene.
            //Create the association between this intersection and it.
            secondEdge.AttachIntersection( this );
            AttachEdge( secondEdge );
        }

        //Attempt to find the third adjacent hex
        Hex thirdHex = BoardController.FindHexInScene( _thirdHexX, _thirdHexY );

        if( thirdHex != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this intersection and it.
            if( hexesAreCw )
            {
                thirdHex.AttachIntersection( this, thirdHexDirectionIndex );
            }
            else
            {
                thirdHex.AttachIntersection( this, Hex.GetCcwDirectionIndex( thirdHexDirectionIndex ) );
            }
            AttachHex( thirdHex );
        }

        //Attempt to find the edge between the third and first hexes
        Edge thirdEdge = BoardController.FindEdgeInScene( _thirdHexX, _thirdHexY
                                                        , _firstHexX, _firstHexY );

        if( thirdEdge != null )
        {
            //We found an already created network edge in the scene.
            //Create the association between this intersection and it.
            thirdEdge.AttachIntersection( this );
            AttachEdge( thirdEdge );
        }
    }

    public bool IsAdjacentToHex( int hexCoordinateX, int hexCoordinateY )
    {
        return ((_firstHexX == hexCoordinateX) && (_firstHexY == hexCoordinateY))
            || ((_secondHexX == hexCoordinateX) && (_secondHexY == hexCoordinateY))
            || ((_thirdHexX == hexCoordinateX) && (_thirdHexY == hexCoordinateY));
    }

    public List<Intersection> GetAdjacentIntersections( )
    {
        List<Intersection> adjIntersections = new List<Intersection>( );

        foreach( Edge edge in _edges )
        {
			foreach( Intersection edgeIntersection in edge.Intersections )
            {
                //The edge will have 2 intersections adjacent to it. One of them is this,
                //we want to return the other one as adjacent to this one.
                if( !edgeIntersection.Equals( this ) )
                {
                    adjIntersections.Add( edgeIntersection );
                }
            }
        }

        return adjIntersections;
    }

    //Attach a hex object.
    //Does nothing if the hex is already attached 
    //or there are already 2 hexes attached.
    public void AttachEdge( Edge edge )
    {
        for( int i = 0; i < MAX_ADJ_EDGES; i++ )
        {
            if( i == _edges.Count )
            {
                _edges.Add( edge );
                break;
            }

            if( _edges[i].Equals( edge ) )
            {
                //hex already attached
                break;
            }
        }
    }

    //Attach a hex object.
    //Does nothing if the hex is already attached 
    //or there are already 2 hexes attached.
    public void AttachHex( Hex hex )
    {
        for( int i = 0; i < MAX_ADJ_HEXES; i++ )
        {
            if( i == _hexes.Count )
            {
                _hexes.Add( hex );
                break;
            }

            if( _hexes[i].Equals( hex ) )
            {
                //hex already attached
                break;
            }
        }
    }

    public bool OnLand( )
    {
        foreach( Hex hex in _hexes )
        {
            if( hex != null )
            {
                if( hex.terrainKind != TerrainKind.Sea && hex.terrainKind != TerrainKind.Lake )
                {
                    return true;
                }
            }
        }
        return false;
    }

	// checks whether this intersection contains an opponent knight inferior in strength to the given knight
	public bool HasInferiorKnight(Knight intrudingKnight) {
		Knight existingKnight = GetPiece<Knight> ();
		// if there is a knight at this intersection
		if (existingKnight != null) {
			// if the knight on the intersection belongs to an opponent and has strength lower that the player's knight
			if (existingKnight.Owner != intrudingKnight.Owner && intrudingKnight.Strength > existingKnight.Strength) {
				return true;
			}
		}
		return false;
	}

	// checks whether this intersection is connected to the destination intersection by a route belonging to the given player
	public bool ConnectedByRoute(Intersection destination, PlayerController player) {
		IPriorityQueue<Intersection> unexploredIntersections = new HeapPriorityQueue<Intersection> ();

		unexploredIntersections.Enqueue (this, 0);

		Intersection current;
		while (unexploredIntersections.Count > 0) {
			current = unexploredIntersections.Dequeue ();

			if (current == destination) {
				return true;
			}

			foreach (Edge edge in current.Edges) {
				if (edge != null && edge.HasPieceOwnedByPlayer<Route> (player)) {
					foreach (Intersection intersection in edge.Intersections) {
						if (intersection != current) {
							int priority = Mathf.RoundToInt(Vector3.Distance (intersection.transform.position, destination.transform.position));
							unexploredIntersections.Enqueue(intersection, priority);
						}
					}
				}
			}
		}

		return false;
    }

    public override List<SerializableHex> AsSerializable( )
    {
        List<SerializableHex> output = new List<SerializableHex>( );

        foreach( Hex hex in _hexes )
        {
            SerializableHex serializableHex = new SerializableHex( );
            serializableHex._CoordinateX = hex._CoordinateX;
            serializableHex._CoordinateY = hex._CoordinateY;
            serializableHex.diceNumber = hex.diceNumber;
            serializableHex.terrainKind = hex.terrainKind;
            output.Add( serializableHex );
        }

        return output;
    }
}
