﻿using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class Edge : Location
{
    //Adjacent intersections
    public static readonly int MAX_ADJ_INTERSECTIONS = 2;
    private List<Intersection> _intersections = new List<Intersection>( );
    public List<Intersection> Intersections
    {
        get
        {
            return _intersections;
        }
        private set
        {
            _intersections = value;
        }
    }

    //Adjacent hexes; 
    //Be sure to check for null, an edge can have 1-2 adjacent hexes.
    public static readonly int MAX_ADJ_HEXES = 2;
    private List<Hex> _hexes = new List<Hex>( );
    public List<Hex> Hexes
    {
        get
        {
            return _hexes;
        }
        private set
        {
            _hexes = value;
        }
    }

    //We can determine an edge's position on the board based on the coordinates
    //of the two hexes adjacent to it.
    //These are used on clients to find adjacent networked intersections/hexes.
    [SyncVar]
    public int _firstHexX;

    [SyncVar]
    public int _firstHexY;

    [SyncVar]
    public int _secondHexX;

    [SyncVar]
    public int _secondHexY;

    protected void Start( )
    {
        if( !isServer )
        {
            MakeAssociations( );
        }

        transform.parent = GameObject.Find( "Board/Edges" ).transform;
    }

    //This method is meant to be called by non-server clients to create the
    //necessary associations between all of the networked board locations.
    [Client]
    protected override void MakeAssociations( )
    {
        BoardController controller = Controller;
        if( controller != null )
        {
            controller.RegisterLocation( this );
        }

        //Get the direction from the first Hex to the second Hex
        Tuple<int, int> firstToSecond = new Tuple<int, int>( _secondHexX - _firstHexX
                                                           , _secondHexY - _firstHexY );

        //Attempt to find the first adjacent hex
        Hex firstHex = BoardController.FindHexInScene( _firstHexX, _firstHexY );
        int hexDirectionIndex = Hex.GetDirectionIndex( firstToSecond );

        if( firstHex != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this hex and the adjacent one.
            firstHex.AttachEdge( this, hexDirectionIndex );
            AttachHex( firstHex );
        }

        //Attempt to find the second adjacent hex
        Hex secondHex = BoardController.FindHexInScene( _secondHexX, _secondHexY );
        int revDirectionIndex = (hexDirectionIndex + 3) % 6;

        if( secondHex != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this hex and the adjacent one.
            secondHex.AttachEdge( this, revDirectionIndex );
            AttachHex( firstHex );
        }

        //Attempt to find the intersection that is CCW to the edge relative to
        //the first hex.
        int ccwDirectionIndex = Hex.GetCcwDirectionIndex( hexDirectionIndex );
        Tuple<int, int> ccwDirection = Hex.Directions[ccwDirectionIndex];
        Intersection firstIntersection = BoardController.FindIntersectionInScene( _firstHexX
                                                                                , _firstHexY
                                                                                , _secondHexX
                                                                                , _secondHexY
                                                                                , _firstHexX + ccwDirection.Item1
                                                                                , _firstHexY + ccwDirection.Item2 );

        if( firstIntersection != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this hex and the adjacent one.
            firstIntersection.AttachEdge( this );
            AttachIntersection( firstIntersection );
        }

        //Attempt to find the intersection that is CCW to the edge relative to
        //the first hex.
        int cwDirectionIndex = Hex.GetCwDirectionIndex( hexDirectionIndex );
        Tuple<int, int> cwDirection = Hex.Directions[cwDirectionIndex];
        Intersection secondIntersection = BoardController.FindIntersectionInScene( _firstHexX
                                                                                , _firstHexY
                                                                                , _secondHexX
                                                                                , _secondHexY
                                                                                , _firstHexX + cwDirection.Item1
                                                                                , _firstHexY + cwDirection.Item2 );

        if( secondIntersection != null )
        {
            //We found an already created network hex in the scene.
            //Create the association between this hex and the adjacent one.
            secondIntersection.AttachEdge( this );
            AttachIntersection( secondIntersection );
        }
    }

    public bool IsAdjacentToHex( int hexCoordinateX, int hexCoordinateY )
    {
        return ((_firstHexX == hexCoordinateX) && (_firstHexY == hexCoordinateY))
            || ((_secondHexX == hexCoordinateX) && (_secondHexY == hexCoordinateY));
    }

    // method to get the intersection shared between this edge and the given edge
    // returns null if the no shared intersection
    public Intersection GetSharedIntersection( Edge edge )
    {
        Intersection output = null;

        foreach( Intersection thisIntersection in _intersections )
        {
            foreach( Intersection otherIntersection in edge._intersections )
            {
                if( thisIntersection == otherIntersection )
                {
                    output = thisIntersection;
                }
            }
        }

        return output;
    }

    // method that returns a list of edges adjacent to this edge
    public List<Edge> GetAdjacentEdges( )
    {
        List<Edge> adjEdges = new List<Edge>( );

        foreach( Intersection intersection in _intersections )
        {
            foreach( Edge edge in intersection.Edges )
            {
                if( edge != null && edge != this )
                {
                    adjEdges.Add( edge );
                }
            }
        }

        return adjEdges;
    }

    //Attach an intersection object.
    public void AttachIntersection( Intersection intersection )
    {
        for( int i = 0; i < MAX_ADJ_INTERSECTIONS; i++ )
        {
            if( i == _intersections.Count )
            {
                _intersections.Add( intersection );
                break;
            }

            if( _intersections[i].Equals( intersection ) )
            {
                //intersection already attached
                break;
            }
        }
    }

    //Attach a hex object.
    //Does nothing if the hex is already attached 
    //or there are already 2 hexes attached.
    public void AttachHex( Hex hex )
    {
        for( int i = 0; i < MAX_ADJ_HEXES; i++ )
        {
            if( i == _hexes.Count )
            {
                _hexes.Add( hex );
                break;
            }

            if( _hexes[i].Equals( hex ) )
            {
                //hex already attached
                break;
            }
        }
    }

    // Method for checking if the edge is on land (has at least one adjacent land hex)
    public bool OnLand( )
    {
        foreach( Hex hex in _hexes )
        {
            if( hex != null )
            {
                if( hex.terrainKind != TerrainKind.Sea && hex.terrainKind != TerrainKind.Lake )
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Method for checking if the edge is in the ocean (has at least one adjacent sea hex)
    public bool InOcean( )
    {
        foreach( Hex hex in _hexes )
        {
            if( hex != null )
            {
                if( hex.terrainKind == TerrainKind.Sea )
                {
                    return true;
                }
            }
        }
        return false;
    }

    public override List<SerializableHex> AsSerializable( )
    {
        List<SerializableHex> output = new List<SerializableHex>( );

        foreach( Hex hex in _hexes )
        {
            SerializableHex serializableHex = new SerializableHex( );
            serializableHex._CoordinateX = hex._CoordinateX;
            serializableHex._CoordinateY = hex._CoordinateY;
            serializableHex.diceNumber = hex.diceNumber;
            serializableHex.terrainKind = hex.terrainKind;
            output.Add( serializableHex );
        }

        return output;
    }
}
