﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BoardController : NetworkBehaviour
{
    public GameObject _boardPrefab;
    public Hex _hexPrefab;
    public Edge _edgePrefab;
    public Intersection _intersectionPrefab;

    //List of all locations.
    private List<Location> _locations = new List<Location>( );

    //We additionally store a second reference for each location in more specific lists,
    //for more optimized searching.
    private List<Hex> _hexes = new List<Hex>( );
    public List<Hex> Hexes
    {
        get
        {
            return _hexes;
        }
    }

    private List<Edge> _edges = new List<Edge>( );
    private List<Intersection> _intersections = new List<Intersection>( );

    public static Hex FindHexInScene( int coordinateX, int coordinateY )
    {
        GameObject[] boardHexes = GameObject.FindGameObjectsWithTag( "Hex" );
        Hex output = null;

        foreach( GameObject hexObject in boardHexes )
        {
            Hex hex = hexObject.GetComponent<Hex>( );
            if( (hex._CoordinateX == coordinateX)
             && (hex._CoordinateY == coordinateY) )
            {
                output = hex;
                break;
            }
        }

        return output;
    }

    public static Edge FindEdgeInScene( int firstCoordinateX, int firstCoordinateY
                                      , int secondCoordinateX, int secondCoordinateY )
    {
        GameObject[] boardEdges = GameObject.FindGameObjectsWithTag( "Edge" );
        Edge output = null;

        foreach( GameObject edgeObject in boardEdges )
        {
            Edge edge = edgeObject.GetComponent<Edge>( );
            if( edge.IsAdjacentToHex( firstCoordinateX, firstCoordinateY )
             && edge.IsAdjacentToHex( secondCoordinateX, secondCoordinateY ) )
            {
                output = edge;
                break;
            }
        }

        return output;
    }

    public static Intersection FindIntersectionInScene( int firstCoordinateX, int firstCoordinateY
                                                      , int secondCoordinateX, int secondCoordinateY
                                                      , int thirdCoordinateX, int thirdCoordinateY )
    {
        GameObject[] boardIntersections = GameObject.FindGameObjectsWithTag( "Intersection" );
        Intersection output = null;

        foreach( GameObject intersectionObject in boardIntersections )
        {
            Intersection intersection = intersectionObject.GetComponent<Intersection>( );
            if( intersection.IsAdjacentToHex( firstCoordinateX, firstCoordinateY )
             && intersection.IsAdjacentToHex( secondCoordinateX, secondCoordinateY )
             && intersection.IsAdjacentToHex( thirdCoordinateX, thirdCoordinateY ) )
            {
                output = intersection;
            }
        }

        return output;
    }

    public void Start( )
    {
        if( isServer )
        {
            SpawnBoard( _boardPrefab );
        }
        else
        {
            //On the client, we assume that when the BoardController's Start is called,
            //all of the locations on the board have been created and exist in the scene
            //Note: They may not have been fully initialized by Unity, but we should
            //      be able to reference the objects themselves.
            MakeAssociations( );
        }
    }

    public void RegisterLocation( Location location )
    {
        _locations.Add( location );

        if( location is Hex )
        {
            _hexes.Add( location as Hex );
        }
        else if( location is Edge )
        {
            _edges.Add( location as Edge );
        }
        else if( location is Intersection )
        {
            _intersections.Add( location as Intersection );
        }
    }

    //This method returns all of the intersections that obey the distance rule.
    //It does NOT check if the intersections are on any trade route networks.
    public List<Intersection> GetSetupTownLocations( Town town )
    {
        List<Intersection> output = new List<Intersection>();

        foreach( Intersection intersection in _intersections )
        {
            if( town.IsValidSetupLocation( intersection ) )
            {
                output.Add( intersection );
            }
        }

        return output;
    }

    public List<Location> GetValidLocations( Piece piece )
    {
        List<Location> output = new List<Location>( );

        foreach( Location location in _locations )
        {
            if( piece.CanPlaceAtLocation( location ) )
            {
                output.Add( location );
            }
        }

        return output;
    }

	public List<Location> GetValidMoveLocations (Piece piece) {
		List<Location> output = new List<Location>( );

		foreach( Location location in _locations )
		{
			if( piece.CanMoveToLocation(location) )
			{
				output.Add( location );
			}
		}

		return output;
	}

	public Location GetRobberLocation() {
		foreach (Location location in _locations) {
			if (location is Hex && (location as Hex).terrainKind == TerrainKind.Desert) {
				return location;
			}
		}

		return null;
	}

	public Location GetPirateLocation() {
		foreach (Location location in _locations) {
			if (location is Hex && (location as Hex).terrainKind == TerrainKind.Sea) {
				return location;
			}
		}

		return null;
	}

    //Searches for an initialized hex at the given axial coordinate.
    //Returns null if there is no initialized hex on the board with that 
    //coordinate.
    private Hex GetHexWithCoordinates( Tuple<int,int> axialCoordinate )
    {
        Hex output = null;

        foreach( Hex hex in _hexes )
        {
            if( (hex._CoordinateX == axialCoordinate.Item1) 
             && (hex._CoordinateY == axialCoordinate.Item2) )
            {
                output = hex;
                break;
            }
        }

        return output;
    }

    //Searches for an initialized edge bordering hexes with the adjacent axial coordinates.
    //Returns null if there no edge could be found on the board with those coordinates.
    private Edge GetEdgeWithCoordinates( Tuple<int, int> axialCoordinate1, Tuple<int, int> axialCoordinate2 )
    {
        Edge output = null;

        foreach( Edge edge in _edges )
        {
            if( (edge.IsAdjacentToHex( axialCoordinate1.Item1, axialCoordinate1.Item2 ) )
             && (edge.IsAdjacentToHex( axialCoordinate2.Item1, axialCoordinate2.Item2 ) ) )
            {
                output = edge;
                break;
            }
        }

        return output;
    }

    //Searches for an initialized edge bordering hexes with the adjacent axial coordinates.
    //Returns null if there no edge could be found on the board with those coordinates.
    private Intersection GetIntersectionWithCoordinates( Tuple<int, int> axialCoordinate1
                                                       , Tuple<int, int> axialCoordinate2
                                                       , Tuple<int, int> axialCoordinate3 )
    {
        Intersection output = null;

        foreach( Intersection intersection in _intersections )
        {
            if( (intersection.IsAdjacentToHex( axialCoordinate1.Item1, axialCoordinate1.Item2 ))
             && (intersection.IsAdjacentToHex( axialCoordinate2.Item1, axialCoordinate2.Item2 ))
             && (intersection.IsAdjacentToHex( axialCoordinate3.Item1, axialCoordinate3.Item2 )) )
            {
                output = intersection;
                break;
            }
        }

        return output;
    }

    //This method is meant to be called by the host/server to spawn all of the
    //board locations that will be used during the game.
    //This method accepts a prefab which consists of a single root GameObject.
    //The root GameObject must have child GameObjects. Each child GameObject must
    //have the Hex script as a component, and have their X and Y coordinate attributes
    //correctly assigned.
    [Server]
    public void SpawnBoard( GameObject prefab )
    {
        //The goal of this function is to iterate over the existing Hex
        //objects that make up the board, and create the appropriate edge and 
        //intersection objects between them.

        // Iterate over all the children of the board (the terrain hexes)
        foreach( Transform child in prefab.transform )
        {
            Hex prefabHex = child.gameObject.GetComponent<Hex>( );

            //Instantiate and spawn a new Hex GameObject based on prefab's child's state.
            Hex currentHex = Instantiate( _hexPrefab );
            currentHex.transform.position = child.transform.position;

            //Setup SyncVars for the Hex based on the board prefab's values
            currentHex.gameObject.SetActive( true );
            currentHex._CoordinateX = prefabHex._CoordinateX;
            currentHex._CoordinateY = prefabHex._CoordinateY;
            currentHex.diceNumber = prefabHex.diceNumber;
            currentHex.terrainKind = prefabHex.terrainKind;
            NetworkServer.Spawn( currentHex.gameObject );

            for( int directionIndex = 0; directionIndex < Hex.Directions.Count; directionIndex++ )
            {
                //Attempt to find an already initialized hex in the direction.
                Tuple<int, int> direction = Hex.Directions[directionIndex];
                Tuple<int, int> adjHexPosition = new Tuple<int, int>( currentHex._CoordinateX + direction.Item1
                                                                    , currentHex._CoordinateY + direction.Item2 );
                Hex adjHex = GetHexWithCoordinates( adjHexPosition );

                if( adjHex != null )
                {
                    //We found a hex in our networked list.
                    //This hex must have already made edges and vertices that
                    //will end up between it and currentHex. We need to get
                    //references to these objects and attach them.

                    //These indices will point to the intersections we want from the adjHex.
                    int revDirectionIndex = (directionIndex + 3) % 6;
                    int revCcwDirectionIndex = Hex.GetCcwDirectionIndex( revDirectionIndex );

                    //Naming scheme is their direction in relation to direction.
                    Intersection ccwIntersection = adjHex.GetIntersection( revDirectionIndex );
                    Edge edge = adjHex.GetEdge( revDirectionIndex );
                    Intersection cwIntersection = adjHex.GetIntersection( revCcwDirectionIndex );

                    //Add associations
                    currentHex.AttachIntersection( ccwIntersection, Hex.GetCcwDirectionIndex( directionIndex ) );
                    currentHex.AttachEdge( edge, directionIndex );
                    currentHex.AttachIntersection( cwIntersection, directionIndex );

                    ccwIntersection.AttachHex( currentHex );
                    edge.AttachHex( currentHex );
                    cwIntersection.AttachHex( currentHex );
                }
                else
                {
                    //We did not find an adjacent hex in our SyncList,
                    //therefore it has not been spawned on the network yet.

                    //We must create a new edge, but other hexes adjacent to
                    //currentHex may have made intersections. We attempt to
                    //find these intersections first, and then create them if
                    //we cannot find them. Finally, attach them all to
                    //currentHex.

                    //Check for the existence of the intersection CCW to this edge.
                    //
                    //We already know the hex at adjHexPosition doesn't exist, 
                    //so it can't hold a reference to the intersection we're looking for
                    //
                    //However, we also need to check the hex located in the ccwDirection
                    //to see if it has a reference to the intersection we're looking for.
                    int ccwDirectionIndex = Hex.GetCcwDirectionIndex( directionIndex );
                    Intersection ccwIntersection = currentHex.GetIntersection( ccwDirectionIndex );

                    if( ccwIntersection == null )
                    {
                        //We don't find it on our hex, check the hex in the ccwDirection.
                        Tuple<int, int> ccwDirection = Hex.Directions[ccwDirectionIndex];
                        Tuple<int, int> ccwHexPosition = new Tuple<int, int>( currentHex._CoordinateX + ccwDirection.Item1
                                                                            , currentHex._CoordinateY + ccwDirection.Item2 );
                        Hex ccwHex = GetHexWithCoordinates( ccwHexPosition );
                        if( ccwHex != null )
                        {
                            //The CCW hex has already been spawned. Since it has been spawned,
                            //its intersections have also already been spawned. Get the
                            //intersection we want from it.

                            //We can get the index of the intersection we want on ccwHex by 
                            //reversing the direction index, and then moving one position CCW.
                            int intersectionIndex = Hex.GetCcwDirectionIndex( (ccwDirectionIndex + 3) % 6 );
                            ccwIntersection = ccwHex.GetIntersection( intersectionIndex );
                        }
                        else
                        {
                            //The CCW hex has not been spawned. Therefore, we are the first hex to
                            //need the intersection, and must spawn it.
                            //TODO (eric): Somehow get the harbour information for this intersection.
                            ccwIntersection = Instantiate( _intersectionPrefab );

                            //Set the position of the intersection
                            int angle = 30 + 60 * ccwDirectionIndex;
                            Vector3 distanceToIntersection = Vector3.up * Hex.outerRadius;
                            //We want a quaternion that will rotate the up vector "angle" degrees
                            //about the z-axis. Vector3.back is going out the screen, and rotation
                            //follows the left hand rule, so it looks like a CCW rotation as you
                            //look at the screen.
                            Quaternion rotation = Quaternion.AngleAxis( angle, Vector3.back );
                            ccwIntersection.transform.position = child.transform.position
                                                               + rotation * distanceToIntersection;

                            //Setup SyncVars for the Intersection based on the surrounding hexes
                            ccwIntersection._firstHexX = currentHex._CoordinateX;
                            ccwIntersection._firstHexY = currentHex._CoordinateY;
                            ccwIntersection._secondHexX = adjHexPosition.Item1;
                            ccwIntersection._secondHexY = adjHexPosition.Item2;
                            ccwIntersection._thirdHexX = ccwHexPosition.Item1;
                            ccwIntersection._thirdHexY = ccwHexPosition.Item2;
                            NetworkServer.Spawn( ccwIntersection.gameObject );

                            //Keep track of all spawned intersections
                            _intersections.Add( ccwIntersection );
                            _locations.Add( ccwIntersection );
                        }

                        //Create the association between the intersection and this hex.
                        currentHex.AttachIntersection( ccwIntersection, ccwDirectionIndex );
                        ccwIntersection.AttachHex( currentHex );
                    }

                    //Check for the existence of the intersection CW to this edge.
                    //We already know the hex at adjHexPosition doesn't exist, so it can't hold a
                    //reference to the intersection we're looking for.
                    //However, we also need to check the hex located in the cwDirection to see if
                    //it has a reference to the intersection we're looking for.
                    Intersection cwIntersection = currentHex.GetIntersection( directionIndex );

                    if( cwIntersection == null )
                    {
                        //We don't find it on our hex, check the hex in the cwDirection.
                        int cwDirectionIndex = Hex.GetCwDirectionIndex( directionIndex );
                        Tuple<int, int> cwDirection = Hex.Directions[cwDirectionIndex];
                        Tuple<int, int> cwHexPosition = new Tuple<int, int>( currentHex._CoordinateX + cwDirection.Item1
                                                                           , currentHex._CoordinateY + cwDirection.Item2 );
                        Hex cwHex = GetHexWithCoordinates( cwHexPosition );
                        if( cwHex != null )
                        {
                            //The CW hex has already been spawned. Since it has been spawned, its
                            //intersections have also already been spawned. Get the intersection
                            //we want from it.
                            //We can get the index of the intersection we want on cwHex by
                            //reversing the direction index.
                            int intersectionIndex = (cwDirectionIndex + 3) % 6;
                            cwIntersection = cwHex.GetIntersection( intersectionIndex );
                        }
                        else
                        {
                            //We could not find hexes that would be adajacent to this intersection.
                            //Therefore, we are the first hex to need the intersection, and must create it.
                            //TODO (eric): Somehow get the harbour information for this intersection.
                            cwIntersection = Instantiate( _intersectionPrefab );

                            //Set the position of the intersection
                            int angle = 30 + 60 * directionIndex;
                            Vector3 distanceToIntersection = Vector3.up * Hex.outerRadius;
                            //We want a quaternion that will rotate the up vector "angle" degrees
                            //about the z-axis. Vector3.back is going out the screen, and rotation
                            //follows the left hand rule, so it looks like a CCW rotation as you
                            //look at the screen.
                            Quaternion rotation = Quaternion.AngleAxis( angle, Vector3.back );
                            cwIntersection.transform.position = child.transform.position
                                                              + rotation * distanceToIntersection;

                            //Setup SyncVars for the Intersection based on the surrounding hexes
                            cwIntersection._firstHexX = currentHex._CoordinateX;
                            cwIntersection._firstHexY = currentHex._CoordinateY;
                            cwIntersection._secondHexX = adjHexPosition.Item1;
                            cwIntersection._secondHexY = adjHexPosition.Item2;
                            cwIntersection._thirdHexX = cwHexPosition.Item1;
                            cwIntersection._thirdHexY = cwHexPosition.Item2;
                            NetworkServer.Spawn( cwIntersection.gameObject );

                            //Keep track of all created intersections
                            _intersections.Add( cwIntersection );
                            _locations.Add( cwIntersection );
                        }

                        //Create the association between the intersection and this hex.
                        currentHex.AttachIntersection( cwIntersection, directionIndex );
                        cwIntersection.AttachHex( currentHex );
                    }

                    //Instantiate and spawn a new edge for this side of the hex.
                    Edge edge = Instantiate( _edgePrefab );

                    //Set the position of the edge
                    int edgeAngle = 60 * directionIndex;
                    Vector3 distanceToEdge = Vector3.up * Hex.innerRadius;
                    //We want a quaternion that will rotate the up vector "angle" degrees
                    //about the z-axis. Vector3.back is going out the screen, and rotation
                    //follows the left hand rule, so it looks like a CCW rotation as you
                    //look at the screen.
                    Quaternion edgeRotation = Quaternion.AngleAxis( edgeAngle, Vector3.back );
                    edge.transform.position = child.transform.position
                                            + edgeRotation * distanceToEdge;

                    //Set the rotation of the edge.
                    Vector3 init = cwIntersection.transform.position;
                    Vector3 to = ccwIntersection.transform.position;

                    //if the absolute difference of angle in y is not big -> angle 180.
                    if( Mathf.Abs( init.y - to.y ) < 0.2 )
                    {
                        edge.transform.eulerAngles = new Vector3( 0, 0, 90 );
                    }
                    else
                    {
                        //if the angle is 60 degrees. i.e. incr x + y OR decr x + y
                        if( (init.x < to.x && init.y < to.y) || (init.x > to.x && init.y > to.y) )
                        {
                            edge.transform.eulerAngles = new Vector3( 0, 0, -30 );
                        }
                        //if angle is 120 degrees. decr x + incr y OR incr x or decr y
                        else
                        {
                            edge.transform.eulerAngles = new Vector3( 0, 0, 30 );
                        }
                    }

                    //Setup SyncVars for the Edge based on the surrounding hexes
                    edge._firstHexX = currentHex._CoordinateX;
                    edge._firstHexY = currentHex._CoordinateY;
                    edge._secondHexX = adjHexPosition.Item1;
                    edge._secondHexY = adjHexPosition.Item2;
                    NetworkServer.Spawn( edge.gameObject );

                    // Now that the object is spawned in the network, add it to our
                    // synchronized list.
                    _edges.Add( edge );
                    _locations.Add( edge );

                    //Create the associations between the intersection and the new edge.
                    edge.AttachIntersection( cwIntersection );
                    cwIntersection.AttachEdge( edge );

                    //Create the associations between the intersection and the new edge.
                    edge.AttachIntersection( ccwIntersection );
                    ccwIntersection.AttachEdge( edge );

                    //Create the association between the edge and this hex.
                    currentHex.AttachEdge( edge, directionIndex );
                    edge.AttachHex( currentHex );
                }
            }

            // Keep track of the spawned hexes
            _hexes.Add( currentHex );
            _locations.Add( currentHex );
        }
    }

    //This method is meant to be called by non-server clients to keep track of
    //all of the the networked board locations.
    //This will find all of the network locations that are currently in the
    //scene. When individual locations are added to the scene, they will
    //register themselves with the BoardController, so this doesn't have to be
    //called repeatedly.
    [Client]
    private void MakeAssociations( )
    {
        GameObject[] boardHexes = GameObject.FindGameObjectsWithTag( "Hex" );

        foreach( GameObject hexObject in boardHexes )
        {
            Hex hex = hexObject.GetComponent<Hex>( );
            _locations.Add( hex );
            _hexes.Add( hex );
        }

        GameObject[] boardEdges = GameObject.FindGameObjectsWithTag( "Edge" );

        foreach( GameObject edgeObject in boardEdges )
        {
            Edge edge = edgeObject.GetComponent<Edge>( );
            _locations.Add( edge );
            _edges.Add( edge );
        }

        GameObject[] boardIntersections = GameObject.FindGameObjectsWithTag( "Intersection" );

        foreach( GameObject intersectionObject in boardIntersections )
        {
            Intersection intersection = intersectionObject.GetComponent<Intersection>( );
            _locations.Add( intersection );
            _intersections.Add( intersection );
        }
    }

    public List<Hex> GetHexesWithNumber( int number )
    {
        List<Hex> output = new List<Hex>( );

        foreach( Hex hex in _hexes )
        {
            if( hex.diceNumber == number )
            {
                output.Add( hex );
            }
        }

        return output;
    }

    public Location GetLocationFromSerializable( List<SerializableHex> serializedLocation )
    {
        Location output = null;

        if( serializedLocation.Count == 1 )
        {
            output = GetHexWithCoordinates( new Tuple<int, int>( serializedLocation[0]._CoordinateX
                                                               , serializedLocation[0]._CoordinateY ) );
        }
        else if( serializedLocation.Count == 2 )
        {
            output = GetEdgeWithCoordinates( new Tuple<int, int>( serializedLocation[0]._CoordinateX
                                                                , serializedLocation[0]._CoordinateY )
                                           , new Tuple<int, int>( serializedLocation[1]._CoordinateX
                                                                , serializedLocation[1]._CoordinateY ) );
        }
        else if( serializedLocation.Count == 3 )
        {
            output = GetIntersectionWithCoordinates( new Tuple<int, int>( serializedLocation[0]._CoordinateX
                                                                        , serializedLocation[0]._CoordinateY )
                                                   , new Tuple<int, int>( serializedLocation[1]._CoordinateX
                                                                        , serializedLocation[1]._CoordinateY )
                                                   , new Tuple<int, int>( serializedLocation[2]._CoordinateX
                                                                        , serializedLocation[2]._CoordinateY ) );
        }

        return output;
    }
}

