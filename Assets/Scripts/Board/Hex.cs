﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Networking;

// Class representing a hex on the board
public class Hex : Location
{
    // A visualization of the hexagonal coordinate system.
    // Note that the "axes" pass through the center of edges.
    //
    //      +y direction
    //         /---\
    //        / 0,1 \
    //   /---\\     //---\
    //  /-1,0 \\---// 1,1 \
    //  \     //---\\     /
    //   \---// 0,0 \\---/
    //   /---\\     //---\
    //  /-1,-1\\---// 1,0 \
    //  \     //---\\     /
    //   \---// 0,-1\\---/ +x direction
    //        \     /
    //         \---/

    // The six basic hexagonal directions
    public static readonly Tuple<int, int> DirectionN = new Tuple<int, int>( 0, 1 );
    public static readonly Tuple<int, int> DirectionNE = new Tuple<int, int>( 1, 1 );
    public static readonly Tuple<int, int> DirectionSE = new Tuple<int, int>( 1, 0 );
    public static readonly Tuple<int, int> DirectionS = new Tuple<int, int>( 0, -1 );
    public static readonly Tuple<int, int> DirectionSW = new Tuple<int, int>( -1, -1 );
    public static readonly Tuple<int, int> DirectionNW = new Tuple<int, int>( -1, 0 );

    // An iterable, ordered list of the directions.
    public static readonly List<Tuple<int, int>> Directions = new List<Tuple<int, int>> { DirectionN
                                                                                        , DirectionNE
                                                                                        , DirectionSE
                                                                                        , DirectionS
                                                                                        , DirectionSW
                                                                                        , DirectionNW };

	/*For hexagonal grid generation*/
	// DO NOT REMOVE
	/***********************
	public override Vector3 GetCellDimensions() 
	{
		return gameObject.transform.Find("pPipe1").GetComponent<Renderer> ().bounds.size;	
	}

	public override void MarkAsHighlighted()
	{
		
	}

	public override void MarkAsPath()
	{

	}

	public override void MarkAsReachable() 
	{
	
	}

	public override void UnMark()
	{
	
	}
	*************************/

    // Gets the direction that is counter clockwise to the direction indicated 
    //by the supplied index. For example, supplying a 1 (which indicates the
    //direction NE) will return 0 (which indicates the direction N).
    //Note that direction indexes range from 0 to 5.
    public static int GetCcwDirectionIndex( int hexDirectionIndex )
    {
        int output = hexDirectionIndex - 1;

        if( output < 0 )
        {
            output = 5;
        }

        return output;
    }

    public static int GetCwDirectionIndex( int hexDirectionIndex )
    {
        int output = hexDirectionIndex + 1;

        if( output > 5 )
        {
            output = 0;
        }

        return output;
    }

    public static int GetDirectionIndex( Tuple<int, int> hexDirection )
    {
        int output = -1;

        for( int i = 0; i < Directions.Count; i++ )
        {
            if( Directions[i].Item1 == hexDirection.Item1
             && Directions[i].Item2 == hexDirection.Item2 )
            {
                output = i;
                break;
            }
        }

        return output;
    }

    // set the outer radius of hexagon to any value
    public const float outerRadius = 1.0f;

    // inner radius of hexagon can be calculated from the outer radius
    public const float innerRadius = outerRadius * 0.866025404f;

    // A visualization of the stored edge indexes.
    //        0
    //    5 /---\ 1
    //     /     \
    //     \     /
    //    4 \---/ 2
    //        3
    public static readonly int MAX_ADJ_EDGES = 6;
    private Dictionary<int, Edge> _edges = new Dictionary<int, Edge>( );
    public Dictionary<int, Edge> Edges
    {
        get
        {
            return _edges;
        }
        private set
        {
            _edges = value;
        }
    }

    // A visualization of the stored intersection indexes.
    //      5   0
    //      /---\ 
    //    4/     \1
    //     \     /
    //      \---/ 
    //      3   2
    public static readonly int MAX_ADJ_INTERSECTIONS = 6;
    private Dictionary<int, Intersection> _intersections = new Dictionary<int, Intersection>( );
    public Dictionary<int, Intersection> Intersections
    {
        get
        {
            return _intersections;
        }
        private set
        {
            _intersections = value;
        }
    }

    //These are public ints, instead of a Tuple<int,int>, so that we can edit their values in the editor for the board prefab.
    //We may want to change this to a Tuple<int,int> in the future when we are generating boards during runtime.
    [SyncVar]
    public int _CoordinateX;
    [SyncVar]
    public int _CoordinateY;

    // The number on the hex
    [SyncVar( hook = "OnDiceNumberChange" )]
    public int diceNumber;

    // The kind of terrain that the hex has, indicated by an enum
    [SyncVar( hook = "OnTerrainKindChange" )]
    public TerrainKind terrainKind;

    public Material _pastureMaterial;
    public Material _forestMaterial;
    public Material _mountainMaterial;
    public Material _hillMaterial;
    public Material _fieldMaterial;
    public Material _goldmineMaterial;
    public Material _seaMaterial;
    public Material _desertMaterial;
    public Material _lakeMaterial;

    protected void Start( )
    {
        //Update the dice number text
        OnDiceNumberChange( diceNumber );

        //Update terrain material
        OnTerrainKindChange( terrainKind );

        if( !isServer )
        {
            MakeAssociations( );
        }

        transform.parent = GameObject.Find( "Board/Hexes" ).transform;
    }

    protected void OnDiceNumberChange( int newNumber )
    {
        //Assign the appropriate shader depending on the hex type.
        TextMesh diceText = transform.Find( "DiceNumber" ).gameObject.GetComponent<TextMesh>( );

        if( (diceNumber >= 2)
         && (diceNumber <= 12) )
        {
            diceText.gameObject.SetActive( true );
            diceText.text = diceNumber.ToString( );
            Bounds textBounds = diceText.GetComponent<Renderer>( ).bounds;
            diceText.transform.position = new Vector3( transform.position.x - (textBounds.max.x - textBounds.min.x) / 2
                                                     , transform.position.y + (textBounds.max.y - textBounds.min.y) / 2
                                                     , -0.1f );
        }
        else
        {
            diceText.gameObject.SetActive( false );
        }
    }

    protected void OnTerrainKindChange( TerrainKind newKind )
    {
        //Assign the appropriate shader depending on the hex type.
        Renderer cylinderRenderer = transform.Find( "pCylinder1" ).gameObject.GetComponent<Renderer>( );

        switch( terrainKind )
        {
            case TerrainKind.Pasture:
                cylinderRenderer.material = _pastureMaterial;
                break;
            case TerrainKind.Forest:
                cylinderRenderer.material = _forestMaterial;
                break;
            case TerrainKind.Mountain:
                cylinderRenderer.material = _mountainMaterial;
                break;
            case TerrainKind.Hill:
                cylinderRenderer.material = _hillMaterial;
                break;
            case TerrainKind.Field:
                cylinderRenderer.material = _fieldMaterial;
                break;
            case TerrainKind.GoldMine:
                cylinderRenderer.material = _goldmineMaterial;
                break;
            case TerrainKind.Sea:
                cylinderRenderer.material = _seaMaterial;
                break;
            case TerrainKind.Desert:
                cylinderRenderer.material = _desertMaterial;
                break;
            case TerrainKind.Lake:
                //No material available yet.
                //cylinderRenderer.material = _lakeMaterial;
                break;
            default:
                break;
        }
    }

    //This method is meant to be called by non-server clients to create the
    //necessary associations between all of the networked board locations.
    [Client]
    protected override void MakeAssociations( )
    {
        BoardController controller = Controller;
        if( controller != null )
        {
            controller.RegisterLocation( this );
        }

        // Check for the existence of adjacent locations
        for( int directionIndex = 0; directionIndex < Hex.Directions.Count; directionIndex++ )
        {
            //Attempt to find a networked Hex in this direction.
            Tuple<int, int> direction = Hex.Directions[directionIndex];
            Tuple<int, int> adjHexPosition = new Tuple<int, int>( _CoordinateX + direction.Item1
                                                                , _CoordinateY + direction.Item2 );
            Hex adjHex = BoardController.FindHexInScene( adjHexPosition.Item1, adjHexPosition.Item2 );

            if( adjHex != null )
            {
                //We found an already created network hex in the scene.
                //Create the association between this hex and the adjacent one.
                //TODO (eric): Do we want to store this association on Hex?
            }

            //Attempt to find a networked Edge in this direction.
            Edge adjEdge = BoardController.FindEdgeInScene( _CoordinateX, _CoordinateY
                                                          , adjHexPosition.Item1, adjHexPosition.Item2 );

            if( adjEdge != null )
            {
                //We found an already created network hex in the scene.
                //Create the association between this hex and the adjacent one.
                adjEdge.AttachHex( this );
                AttachEdge( adjEdge, directionIndex );
            }

            //Attempt to find a networked Intersection in this direction.
            int cwDirectionIndex = Hex.GetCwDirectionIndex( directionIndex );
            Tuple<int, int> cwDirection = Hex.Directions[cwDirectionIndex];
            Tuple<int, int> cwHexPosition = new Tuple<int, int>( _CoordinateX + cwDirection.Item1
                                                               , _CoordinateY + cwDirection.Item2 );
            Intersection adjIntersection = BoardController.FindIntersectionInScene( _CoordinateX
                                                                                  , _CoordinateY
                                                                                  , adjHexPosition.Item1
                                                                                  , adjHexPosition.Item2
                                                                                  , cwHexPosition.Item1
                                                                                  , cwHexPosition.Item2 );

            if( adjIntersection != null )
            {
                //We found an already created network hex in the scene.
                //Create the association between this hex and the adjacent one.
                adjIntersection.AttachHex( this );
                AttachIntersection( adjIntersection, directionIndex );
            }

        }
    }

    //Returns the world position of the passed adjacent intersection
    //Returns the Hex's center position if the intersection passed is
    //not adjacent to the Hex.
    public Vector3 GetIntersectionWorldPosition( Intersection intersection )
    {
        Vector3 output = transform.position;

        //We iterate over our stored intersections and try to find the passed
        //intersection object. We can then determine what angle the intersection
        //is at, given that 0 degrees represents the unit vector (0,1).
        //Since there are 60 degrees between intersections in a hexagon, we can
        //determine the angle of a given intersection index by the following formula:
        //  angle = 30 + 60 * index
        for( int i = 0; i < MAX_ADJ_INTERSECTIONS; i++ )
        {
            if( _intersections[i].Equals( intersection ) )
            {
                int angle = 30 + 60 * i;
                Vector3 distanceToIntersection = Vector3.up * Hex.outerRadius;
                //We want a quaternion that will rotate our Vector3 angle degrees about the z-axis.
                //Vector3.back is going out the screen, 
                //and rotation follows the left hand rule,
                //so it looks like a CCW rotation as you look at the screen.
                Quaternion rotation = Quaternion.AngleAxis( angle, Vector3.back );
                output += rotation * distanceToIntersection;
                break;
            }
        }

        return output;
    }

    //Returns the world position of the passed adjacent intersection
    //Returns the Hex's center position if the intersection passed is
    //not adjacent to the Hex.
    public Vector3 GetEdgeWorldPosition( Edge edge )
    {
        Vector3 output = transform.position;

        //We iterate over our stored edges and try to find the passed edge
        //object. We can then determine what angle the edge is at, given that
        //0 degrees represents the unit vector (0,1). Since there are 60
        //degrees between intersections in a hexagon, we can determine the
        //angle of a given edge index by the following formula:
        //  angle = 60 * index
        for( int i = 0; i < MAX_ADJ_EDGES; i++ )
        {
            if( _edges[i].Equals( edge ) )
            {
                int angle = 60 * i;
                Vector3 distanceToIntersection = Vector3.up * Hex.innerRadius;
                //We want a quaternion that will rotate our Vector3 angle degrees about the z-axis.
                //Vector3.back is going out the screen, 
                //and rotation follows the left hand rule,
                //so it looks like a CCW rotation as you look at the screen.
                Quaternion rotation = Quaternion.AngleAxis( angle, Vector3.back );
                output += rotation * distanceToIntersection;
                break;
            }
        }

        return output;
    }

    //Get the intersection in the direction specified by the supplied index.
    public Intersection GetIntersection( int hexDirectionIndex )
    {
        Intersection output = null;

        if( _intersections.ContainsKey( hexDirectionIndex ) )
        {
            output = _intersections[hexDirectionIndex];
        }

        return output;
    }

    //Get the edge in the direction specified by the supplied index.
    public Edge GetEdge( int hexDirectionIndex )
    {
        Edge output = null;

        if( _edges.ContainsKey( hexDirectionIndex ) )
        {
            output = _edges[hexDirectionIndex];
        }

        return output;
    }

    //Attach the supplied intersection in the direction specified by the supplied index.
    public void AttachIntersection( Intersection intersection, int hexDirectionIndex )
    {
        _intersections[hexDirectionIndex] = intersection;
    }

    //Attach the supplied edge in the direction specified by the supplied index.
    public void AttachEdge( Edge edge, int hexDirectionIndex )
    {
        _edges[hexDirectionIndex] = edge;
    }

	// determines whether the given player has a town (settlement or city) adjacent to this hex
	public bool HasPlayerTown(PlayerController player) {
		foreach (KeyValuePair<int, Intersection> entry in _intersections) {
			if (entry.Value.HasPieceOwnedByPlayer<Town> (player)) {
				return true;
			}
		}

		return false;
	}

	// determines whether the giben player has a ship adjacent to this hex
	public bool HasPlayerShip(PlayerController player) {
		foreach (KeyValuePair<int, Edge> entry in _edges) {
			if (entry.Value.HasPieceOwnedByPlayer<Ship> (player)) {
				return true;
			}
		}

		return false;
	}

    public override List<SerializableHex> AsSerializable( )
    {
        List<SerializableHex> output = new List<SerializableHex>( );
        SerializableHex serializableHex = new SerializableHex( );
        serializableHex._CoordinateX = _CoordinateX;
        serializableHex._CoordinateY = _CoordinateY;
        serializableHex.diceNumber = diceNumber;
        serializableHex.terrainKind = terrainKind;
        output.Add( serializableHex );
        return output;
    }
}

// enumeration for the kind of terrain the terrainhex is
public enum TerrainKind {
	Pasture,
	Forest,
	Mountain,
	Hill,
	Field,
	GoldMine,
	Sea,
	Desert,
	Lake //added lake 
};

[Serializable]
public class SerializableHex
{
    //The hex position in axial coordinates
    public int _CoordinateX;
    public int _CoordinateY;

    // The number on the hex
    public int diceNumber;

    // The kind of terrain that the hex has, indicated by an enum
    public TerrainKind terrainKind;
}