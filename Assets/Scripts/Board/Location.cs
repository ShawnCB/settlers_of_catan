﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Location : NetworkBehaviour
{
    protected List<Piece> _pieces = new List<Piece>( );

    private BoardController _boardController;
    protected BoardController Controller
    {
        get
        {
            //Attempt to find the BoardController object. Return null if you can't.
            if( _boardController == null )
            {
                GameObject boardControllerObj = GameObject.FindWithTag( "BoardController" );

                if( boardControllerObj != null )
                {
                    _boardController = boardControllerObj.GetComponent<BoardController>( );
                }
            }

            return _boardController;
        }

        set
        {
            _boardController = value;
        }
    }

    protected abstract void MakeAssociations( );

    public void RemovePiece( Piece piece )
    {
        _pieces.Remove( piece );
    }
    
    public void AddPiece( Piece piece )
    {
        _pieces.Add( piece );
    }

    public bool HasPiece<T>( ) where T : Piece
    {
        bool output = false;

        foreach( Piece piece in _pieces )
        {
            if( piece is T )
            {
                output = true;
                break;
            }
        }

        return output;
    }

    public bool HasPieceOwnedByPlayer<T>( PlayerController player ) where T : Piece
    {
        bool output = false;

        foreach( Piece piece in _pieces )
        {
            if( (piece is T)
             && (piece.Owner == player) )
            {
                output = true;
                break;
            }
        }

        return output;
    }

    //Returns the first piece of type found on the location.
    //If the location does not have a piece of the supplied type,
    //then this method returns null.
    public T GetPiece<T>( ) where T : Piece
    {
        T output = null;

        foreach( Piece piece in _pieces )
        {
            if( piece is T )
            {
                output = piece as T;
                break;
            }
        }

        return output;
    }

    public abstract List<SerializableHex> AsSerializable( );
}
