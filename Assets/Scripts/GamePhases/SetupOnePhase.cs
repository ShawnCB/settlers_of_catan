﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class SetupOnePhase : Phase
{
    public SetupOnePhase( ) : base( )
    {

    }

    public override Phase GetNextPhase( )
    {
        if( _owner.AllPlayersSetupOneCompleted( ) )
        {
            //All players have placed their first settlement
            //Move onto setup two
            if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
            {
                return new ActiveSetupTwoPhase( );
            }
            else
            {
                return new PassiveSetupTwoPhase( );
            }
        }
        else
        {
            //We are still doing setup one
            if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
            {
                return new ActiveSetupOnePhase( );
            }
            else
            {
                return new PassiveSetupOnePhase( );
            }
        }
    }
}
