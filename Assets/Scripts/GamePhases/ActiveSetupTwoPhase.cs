﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class for handling the local player's turn
[Serializable]
public class ActiveSetupTwoPhase : SetupTwoPhase
{
    public ActiveSetupTwoPhase( ) : base( )
    {

    }

    public override void OnPhaseStart( )
    {
        _owner.SetupTwo( );
    }

    public override void OnPhaseEnd( )
    {

    }

    public override void DrawGUI( )
    {
        GUI.Label( new Rect( 0, 0, 200, 20 ), "Complete Setup Phase Two" );
    }
}
