﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class for handling the local player off turn
[Serializable]
public class PassiveSetupTwoPhase : SetupTwoPhase
{
    public PassiveSetupTwoPhase( ) : base( )
    {

    }

    public override void OnPhaseStart( )
    {
        DebugConsole.Log( "Off Turn" );
    }

    public override void OnPhaseEnd( )
    {

    }

    public override void DrawGUI( )
    {
        //The local player is not the active player.
        GUI.Label( new Rect( 0, 0, 200, 20 ), "Waiting for other player actions" );
    }
}
