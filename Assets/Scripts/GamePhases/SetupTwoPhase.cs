﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class SetupTwoPhase : Phase
{
    public SetupTwoPhase( ) : base( )
    {

    }

    public override Phase GetNextPhase( )
    {
        if( _owner.AllPlayersSetupTwoCompleted( ) )
        {
            //All players have placed their first settlement
            //Move onto main game
            if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
            {
                return new ActiveMainPhase( );
            }
            else
            {
                return new PassiveMainPhase( );
            }
        }
        else
        {
            //We are still doing setup two
            if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
            {
                return new ActiveSetupTwoPhase( );
            }
            else
            {
                return new PassiveSetupTwoPhase( );
            }
        }
    }
}
