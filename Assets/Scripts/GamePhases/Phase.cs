using UnityEngine;
using System;

[Serializable]
public abstract class Phase
{
    [NonSerialized]
    protected GameController _owner;

    [NonSerialized]
    protected UIManager _uiManager;

    public Phase( )
    {
        _owner = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );
        _uiManager = GameObject.Find( "UIManager" ).GetComponent<UIManager>( );
    }

    abstract public void OnPhaseStart( );

    abstract public void OnPhaseEnd( );

    abstract public Phase GetNextPhase( );
    
    abstract public void DrawGUI( );
}