﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class MainPhase : Phase
{
    public MainPhase( ) : base( )
    {

    }

    public override Phase GetNextPhase( )
    {
		_owner.GetLocalPlayer ().movedShip = false;

		foreach (Knight knight in _owner.GetLocalPlayer().PieceManager.GetPiecesOnBoard<Knight>()) {
			knight.WasBuilt = false;
			knight.WasPromoted = false;
		}

        if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
        {
            return new ActiveMainPhase( );
        }
        else
        {
            return new PassiveMainPhase( );
        }
    }
}
