﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class for handling the local player's turn
[Serializable]
public class ActiveMainPhase : MainPhase
{
    private bool action = false;

    // Constructor
    public ActiveMainPhase( ) : base( )
    {

    }

    public override void OnPhaseStart( )
    {
        DebugConsole.Log( "Your Turn" );
        _uiManager._mainPhaseUI.gameObject.SetActive(true);
        _uiManager._mainPhaseUI._rollDiceButton.gameObject.SetActive( true );
    }

    public override void DrawGUI( )
    {

    }

    public override void OnPhaseEnd( )
    {
        _uiManager._mainPhaseUI.gameObject.SetActive( false );
    }

    public bool Action
    {
        get
        {
            return this.action;
        }
        set
        {
            this.action = value;
        }
    }
}
