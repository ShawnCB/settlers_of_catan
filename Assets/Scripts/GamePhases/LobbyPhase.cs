﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class LobbyPhase : Phase
{
    public LobbyPhase( ) : base( )
    {

    }

    public override void OnPhaseStart( )
    {
        _uiManager._lobbyPhaseUI.gameObject.SetActive( true );

        if( NetworkServer.active )
        {
            //Enable the start/load buttons for the host.
            _uiManager._lobbyPhaseUI._loadGameButton.gameObject.SetActive( true );
            _uiManager._lobbyPhaseUI._startMatchButton.gameObject.SetActive( true );
        }
    }

    public override void OnPhaseEnd( )
    {
        _uiManager._lobbyPhaseUI.gameObject.SetActive( false );
    }

    public override Phase GetNextPhase( )
    {
        if( _owner.IsActivePlayer( _owner.GetLocalPlayer( ) ) )
        {
            return new ActiveSetupOnePhase( );
        }
        else
        {
            return new PassiveSetupOnePhase( );
        }
    }

    public override void DrawGUI( )
    {

    }
}
