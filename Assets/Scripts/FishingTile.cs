﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this needs to be always shown somehow on the board
public class FishingTile : MonoBehaviour {

	public int diceNumber; //number on the tile: 4, 5, 6, 7, 8, 9 or 10
	private static readonly int numberOfIntersections = 3;
	private Intersection[] intersections = new Intersection[numberOfIntersections];

	public bool checkCoast(){
		//checks if the intersection is on the coast
		return false;
	}

	public bool checkAdjacents(){
		//checks if the intersections are connected according to the rules 
		return false;
	}
}
