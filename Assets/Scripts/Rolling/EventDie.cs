﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventDie : Die
{
    private Dictionary<int, EventType> rollToEvent = new Dictionary<int, EventType>( ) { { 1, EventType.Trade }
                                                                                       , { 2, EventType.Science }
                                                                                       , { 3, EventType.Politics }
                                                                                       , { 4, EventType.Barbarian }
                                                                                       , { 5, EventType.Barbarian }
                                                                                       , { 6, EventType.Barbarian } };

    protected override void Start( )
    {
        base.Start( );

        _owner._eventDie = this;
    }

    public enum EventType
    {
        Trade,
        Science,
        Politics,
        Barbarian
    }

    public EventType GetLastRollAsEvent( )
    {
        return rollToEvent[LastRoll];
    }

    public override string ToString( )
    {
        string output = "";

        if( rollToEvent.ContainsKey( LastRoll ) )
        {
            output = Identifier + " Die: " + rollToEvent[LastRoll].ToString( );
        }

        return output;
    }
}
