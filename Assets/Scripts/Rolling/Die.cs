﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Die : NetworkBehaviour
{
    [SyncVar( hook = "OnLastRollChanged" )]
    public int LastRoll = 1;

    [SyncVar]
    public string Identifier;

    protected GameController _owner;

    protected virtual void Start( )
    {
        _owner = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );
        transform.parent = GameObject.Find( "Dice" ).transform;

        if( Identifier == "Yellow" )
        {
            _owner._yellowDie = this;
        }
        else if( Identifier == "Red" )
        {
            _owner._redDie = this;
        }
    }

    public int Roll( )
    {
        LastRoll = UnityEngine.Random.Range( 1, 7 );
        return LastRoll;
    }

    public override string ToString( )
    {
        return Identifier + " Die: " + LastRoll;
    }

    public void OnLastRollChanged( int newRoll )
    {
        LastRoll = newRoll;
        _owner.UpdateDiceText( );
    }
}
