﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CommodityCard : Card
{

    private CommodityType Type; //enum property

    public CommodityCard( CommodityType t )
    {
        Type = t;
        Stealable = true;
    }

    public CommodityType type
    { //getter and setter
        get
        {
            return Type;
        }
        set
        {
            Type = value;
        }
    }

    public override bool Equals( object otherCard )
    {
        bool output = false;

        if( (otherCard is CommodityCard)
         && (otherCard as CommodityCard).type == this.type )
        {
            output = true;
        }

        return output;
    }

    public override int GetHashCode( )
    {
        return Type.GetHashCode( );
    }
}

public enum CommodityType
{
    Paper, Cloth, Coin
};

