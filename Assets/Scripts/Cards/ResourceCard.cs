﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ResourceCard : Card
{

    private ResourceType Type; //enum property

    public ResourceCard( )
    {
    }

    public ResourceCard( ResourceType t )
    {
        Type = t;
        Stealable = true;
    }

    public ResourceType type
    { //getter and setter
        get
        {
            return Type;
        }
        set
        {
            Type = value;
        }
    }

    public override bool Equals( object otherCard )
    {
        bool output = false;

        if( (otherCard is ResourceCard)
         && (otherCard as ResourceCard).type == this.type )
        {
            output = true;
        }

        return output;
    }

    public override int GetHashCode( )
    {
        return Type.GetHashCode( );
    }
}

public enum ResourceType
{
    Brick, Grain, Lumber, Ore, Wool, Nothing
};
