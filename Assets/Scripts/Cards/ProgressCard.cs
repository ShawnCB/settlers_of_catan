﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ProgressCard : Card
{

    private ProgressKind Kind;
    private ProgressType Type;

    public ProgressCard( ProgressKind k, ProgressType t )
    {
        Kind = k;
        Type = t;
        Stealable = false;
    }

    public ProgressKind kind
    { //getter and setter
        get
        {
            return kind;
        }
        set
        {
            Kind = value;
        }
    }
    public ProgressType type
    { //getter and setter
        get
        {
            return Type;
        }
        set
        {
            Type = value;
        }
    }

    public void PlayCard( )
    {
        switch( Type )
        {
            case ProgressType.CommercialHarbor:
                break;
            case ProgressType.MasterMerchant:
                break;
            case ProgressType.Merchant:
                break;
            case ProgressType.MerchantFleet:
                break;
            case ProgressType.ResouceMonopoly:
                break;
            case ProgressType.TradeMonopoly:
                break;
            case ProgressType.Bishop:
                break;
            case ProgressType.Constitution:
                break;
            case ProgressType.Deserter:
                break;
            case ProgressType.Diplomat:
                break;
            case ProgressType.Intrigue:
                break;
            case ProgressType.Saboteur:
                break;
            case ProgressType.Spy:
                break;
            case ProgressType.Warlord:
                break;
            case ProgressType.Wedding:
                break;
            case ProgressType.Alchemist:
                break;
            case ProgressType.Crane:
                break;
            case ProgressType.Engineer:
                break;
            case ProgressType.Inventor:
                break;
            case ProgressType.Irrigation:
                break;
            case ProgressType.Medicine:
                break;
            case ProgressType.Mining:
                break;
            case ProgressType.Printer:
                break;
            case ProgressType.RoadBuilding:
                break;
            case ProgressType.Smith:
                break;
        }
    }

    public override bool Equals( object otherCard )
    {
        bool output = false;

        if( ( otherCard is ProgressCard )
         && ( otherCard as ProgressCard ).type == this.type )
        {
            output = true;    
        }

        return output;
    }

    public override int GetHashCode( )
    {
        return Type.GetHashCode( ) + Kind.GetHashCode( );
    }
}

public enum ProgressKind
{
    Trade, Politics, Science
};

public enum ProgressType
{
    //Trade: 2, 2, 6, 2, 4, 2
    CommercialHarbor, MasterMerchant, Merchant, MerchantFleet, ResouceMonopoly, TradeMonopoly,
    //Politics: 2, 1, 2, 2, 2, 2, 3, 2, 2
    Bishop, Constitution, Deserter, Diplomat, Intrigue, Saboteur, Spy, Warlord, Wedding,
    //Science: 2, 2, 1, 2, 2, 2, 2, 1, 2, 2
    Alchemist, Crane, Engineer, Inventor, Irrigation, Medicine, Mining, Printer, RoadBuilding, Smith
};