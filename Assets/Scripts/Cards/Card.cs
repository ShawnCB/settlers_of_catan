﻿using System.Collections;
using System;
using UnityEngine;

[Serializable]
public class Card
{
    public bool Stealable;

    public Card( )
    {
        Stealable = false;
    }
}

