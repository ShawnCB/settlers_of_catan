﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SyncListKeyedNetworkObject : SyncListStruct<KeyedNetworkObject>
{
    public int GetIndex( int key )
    {
        int indexOfExisting = -1;

        foreach( KeyedNetworkObject existingObject in this )
        {
            if( existingObject._key == key )
            {
                indexOfExisting = this.IndexOf( existingObject );
            }
        }

        return indexOfExisting;
    }

    public GameObject GetObject( int key )
    {
        GameObject output = null;

        foreach( KeyedNetworkObject existingObject in this )
        {
            if( existingObject._key == key )
            {
                output = existingObject._object;
            }
        }

        return output;
    }

    public void Add( NetworkBehaviour networkObject, int key )
    {
        this.Add( networkObject.gameObject, key );
    }

    public void Add( GameObject networkObject, int key )
    {
        int indexOfExisting = GetIndex( key );

        if( indexOfExisting != -1 )
        {
            this.RemoveAt( indexOfExisting );
        }

        this.Add( new KeyedNetworkObject { _object = networkObject
                                         , _key = key } );
    }
}
