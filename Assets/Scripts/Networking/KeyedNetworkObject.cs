﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct KeyedNetworkObject
{
    public int _key;
    public GameObject _object;
}
