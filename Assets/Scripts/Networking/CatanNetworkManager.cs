﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class CatanNetworkManager : NetworkManager
{
    public GameController gameControllerPrefab;
    public BoardController boardControllerPrefab;
    public MatchList matchListView;

    public override void OnServerReady( NetworkConnection conn )
    {
        base.OnServerReady( conn );

        //TODO (eric): Find a better place to spawn these. 
        //             It needs to happen once the server is active, 
        //             but this method is also called on clients.
        //             Need a method that is called once on the
        //             server during its startup after its ready.
        if( NetworkServer.active )
        {
            if( GameObject.FindWithTag( "GameController" ) == null )
            {
                NetworkServer.Spawn( Instantiate( gameControllerPrefab.gameObject ) );
            }

            if( GameObject.FindWithTag( "BoardController" ) == null )
            {
                NetworkServer.Spawn( Instantiate( boardControllerPrefab.gameObject ) );
            }
        }
    }

    public override void OnMatchList( bool success, string extendedInfo, List<MatchInfoSnapshot> matchList )
    {
        base.OnMatchList( success, extendedInfo, matchList );
        matchListView.RecreateButtons( matchList );
    }
}
