﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SyncListNetworkObject : SyncListStruct<NetworkObject>
{
    public void Add( NetworkBehaviour networkObject )
    {
        this.Add( new NetworkObject { _object = networkObject.gameObject } );
    }

    public void Add( GameObject networkObject )
    {
        this.Add( new NetworkObject { _object = networkObject } );
    }
}
