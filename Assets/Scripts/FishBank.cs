﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishBank : MonoBehaviour {

	private Dictionary<TokenKind,int> Bank;
	private int totalNumber;
	
	//creates a fish bank
	public void init(){
		Bank.Add (TokenKind.One, 11);
		Bank.Add (TokenKind.Two, 10);
		Bank.Add (TokenKind.Three, 8);
		Bank.Add (TokenKind.Boat, 1);
		totalNumber = 30;
	}

	//player gets a fish token
	public void get(PlayerController p){
		int ones = Bank [TokenKind.One];
		int twos = Bank [TokenKind.Two];
		int threes = Bank [TokenKind.Three];
		int boat = Bank [TokenKind.Boat];
		int random = Random.Range (1, totalNumber);
		if (totalNumber != 0) {
			if (random <= ones) {
				Bank [TokenKind.One] = ones - 1;
			} else if (random > ones && random <= ones + twos) {
				Bank [TokenKind.Two] = twos - 1;
			} else if (random > ones + twos && random <= ones + twos + threes) {
				Bank [TokenKind.Three] = threes - 1;
			} else {
				Bank [TokenKind.Boat] = boat - 1;
			}
			totalNumber -= 1; //reduce totalNumber by 1
		}
		//TODO: player gets a fish token..if(boat)...
	}

	public Dictionary<TokenKind, int> fishbank{
		get{ return Bank; }
		set{ Bank = value; }
	}
}

public enum TokenKind{
	One, Two, Three, Boat
};
