﻿using UnityEngine;
using System;
using System.Collections.Generic;

public abstract class Cell : MonoBehaviour
{
    [HideInInspector]
    [SerializeField]
    private Vector2 _offsetCoord;
    public Vector2 OffsetCoord { get { return _offsetCoord; } set { _offsetCoord = value; } }

    /// <summary>
    /// Method returns distance to other cell, that is given as parameter. 
    /// </summary>
    public abstract int GetDistance(Cell other);

    /// <summary>
    /// Method returns cells adjacent to current cell, from list of cells given as parameter.
    /// </summary>
    public abstract List<Cell> GetNeighbours(List<Cell> cells);
      
    public abstract Vector3 GetCellDimensions(); //Cell dimensions are necessary for grid generators.

    /// <summary>
    ///  Method marks the cell to give user an indication, that selected unit can reach it.
    /// </summary>
    public abstract void MarkAsReachable();
    /// <summary>
    /// Method marks the cell as a part of a path.
    /// </summary>
    public abstract void MarkAsPath();
    /// <summary>
    /// Method marks the cell as highlighted. It gets called when the mouse is over the cell.
    /// </summary>
    public abstract void MarkAsHighlighted();
    /// <summary>
    /// Method returns the cell to its base appearance.
    /// </summary>
    public abstract void UnMark();

}