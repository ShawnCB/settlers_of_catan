﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityWalls : CityPiece
{
    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int> { { new ResourceCard( ResourceType.Brick ), 2 } };
        }
    }

    public override bool CanPlaceAtLocation( Location location )
    {
        bool output = false;

        if( location is Intersection
         && location.HasPieceOwnedByPlayer<City>( Owner ) )
        {
            output = true;
        }

        return output;
    }

	public override bool CanMoveToLocation(Location location) {
		return false;
	}

	public override bool CanMove() {
		return false;
	}
}
