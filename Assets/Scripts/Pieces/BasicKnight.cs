﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicKnight : Knight
{
    public override int Strength
    {
        get
        {
            return 1;
        }
    }

	public override bool CanPlaceAtLocation (Location location) {
		// if the location is an intersection and does not already have piece on it
		if (location is Intersection && !location.HasPiece<Piece> ()) {
			Intersection intersectionLocation = location as Intersection;

			// if the intersection has at least one adjacent land hex
			if (intersectionLocation.OnLand ()) {

				foreach (Edge edge in intersectionLocation.Edges) {

					// if the intersection has an adjacent edge with a route owned by the player
					if (edge != null && edge.HasPieceOwnedByPlayer<Route> (Owner)) {
						return true;
					}
				}
			}
		}
		return false;
	}
		
}