﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MightyKnight : Knight
{
    public override int Strength
    {
        get
        {
            return 3;
        }
    }

	public override bool CanPlaceAtLocation (Location location) {
		// if the location is an intersection and does not already have piece on it
		if (location.HasPieceOwnedByPlayer<StrongKnight> (Owner)) {
			return true;
		}
		return false;
	}
		
}
