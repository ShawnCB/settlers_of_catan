using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//I want to call this route, but I don't know what impact it will have on other classes
public class Road : Route
{
    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int> { { new ResourceCard( ResourceType.Brick ), 1 }
                                             , { new ResourceCard( ResourceType.Lumber ), 1 } };
        }
    }

    // method to check if a road can be placed on this edge by the local player
    public override bool CanPlaceAtLocation( Location location )
    {
        bool connectedToRoute = false;
        bool adjacentToLand = false;

        // Roads can only be placed on Edges that do not already have a Ship or
        // Road on them
        if( location is Edge
         && !location.HasPiece<Route>( ) )
        {
            Edge edgeLocation = location as Edge;
            List<Edge> adjEdgeLocations = edgeLocation.GetAdjacentEdges( );

            //Edge must be next to either a road or town owned by the same player.
            foreach( Edge edge in adjEdgeLocations )
            {
                if( edge.HasPieceOwnedByPlayer<Road>( Owner ) )
                {
                    connectedToRoute = true;
                    break;
                }
            }

            foreach( Intersection intersection in edgeLocation.Intersections )
            {
                if( intersection.HasPieceOwnedByPlayer<Town>( Owner ) )
                {
                    connectedToRoute = true;
                    break;
                }
            }

            //Edge must be next to a non-sea hex.
			if (edgeLocation.OnLand()) {
				adjacentToLand = true;
			}
        }

        return connectedToRoute && adjacentToLand;
    }

	public override bool CanMoveToLocation(Location location) {
		return false;
	}

	public override bool CanMove() {
		return false;
	}
}
