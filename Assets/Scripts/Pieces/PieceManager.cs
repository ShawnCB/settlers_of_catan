﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceManager
{
    private List<Piece> _pieces = new List<Piece>( );

    public void AddPiece( Piece piece )
    {
        _pieces.Add( piece );
    }

    public T GetFreePiece<T>( ) where T : Piece
    {
        T output = null;

        foreach( Piece piece in _pieces )
        {
            if( (piece is T)
             && (piece.Location == null) )
            {
                output = piece as T;
            }
        }

        return output;
    }

    public List<T> GetPieces<T>( ) where T : Piece
    {
        List<T> output = new List<T>( );

        foreach( Piece piece in _pieces )
        {
            if( piece is T )
            {
                output.Add( piece as T );
            }
        }

        return output;
    }

    public List<T> GetPiecesOnBoard<T>( ) where T : Piece
    {
        List<T> output = new List<T>( );

        foreach( Piece piece in _pieces )
        {
            if( (piece is T)
             && (piece.Location != null) )
            {
                output.Add( piece as T );
            }
        }

        return output;
    }

	public T GetPieceOnBoard<T>() where T : Piece {
		foreach( Piece piece in _pieces )
		{
			if( (piece is T)
				&& (piece.Location != null) )
			{
				return piece as T;
			}
		}
		return null;
	}

}
