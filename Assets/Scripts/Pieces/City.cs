﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : Town
{
	public Interactable DiscardInteractable;

    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int> { { new ResourceCard( ResourceType.Ore ), 3 }
                                             , { new ResourceCard( ResourceType.Grain ), 2 } };
        }
    }

    //This method checks if a settlement can be placed on this intersection by the local player
    public override bool CanPlaceAtLocation( Location location )
    {
        bool output = false;

		if( location.HasPieceOwnedByPlayer<Settlement> (Owner) )
        {
            output = true;
        }

        return output;
    }

	public override bool CanMoveToLocation(Location location) {
		return false;
	}

	public override bool CanMove() {
		return false;
	}
}
