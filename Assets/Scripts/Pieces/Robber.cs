﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Robber : HexPiece
{
    //not that Robber is not activated until first barbarian attack
	[SyncVar]
    private bool activate = false;

    public override Dictionary<Card, int> Cost
    {
        get
        {
            //You cannot build a robber, so its cost is free.
            return new Dictionary<Card, int>( );
        }
    }

    //makes the Robber activated for usage after the first barbarian roll
    public void Activate( )
    {
        activate = true;
    }

    //check activation for moving Robber
    public bool isActivated( )
    {
        return activate;
    }

	public override bool CanPlaceAtLocation( Location location )
	{
		bool output = false;

		if( (location is Hex) && (!location.HasPiece<Robber>( )) )
		{
			Hex hexLocation = location as Hex;
			if (hexLocation.terrainKind != TerrainKind.Sea && hexLocation.terrainKind != TerrainKind.Lake) {
				output = true;
			}
		}

		return output;
	}

	public override bool CanMoveToLocation(Location location) {
		bool output = false;

		if( (location is Hex) && (!location.HasPiece<Robber>( )) )
		{
			Hex hexLocation = location as Hex;
			if (hexLocation.terrainKind != TerrainKind.Sea && hexLocation.terrainKind != TerrainKind.Lake) {
				output = true;
			}
		}

		return output;;
	}

	public override bool CanMove() {
		return true;
	}

    public override SerializablePiece AsSerializable( )
    {
        SerializableRobber output = new SerializableRobber( );
        output._location = Location.AsSerializable( );
        output.playerId = PlayerController.ColorToId( Owner.Color );
        output.activate = activate;
        return output;
    }
}

[Serializable]
public class SerializableRobber : SerializablePiece
{   
    //not that Robber is not activated until first barbarian attack
    public bool activate = false;

}
