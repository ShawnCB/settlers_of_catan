﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Town : IntersectionPiece
{
    //Checks if a location obeys the Catan distance rule.
    //The Distance rule applies to intersections, and requires that the intersection and all of
    //its adjacent intersections do not have a town on them.
    //NOTE: Does not check if the location is occupied by any piece other than an existing town.
    //      Use the method CanPlaceAtLocation to check if the location is valid for placement
    //      during main phase gameplay.
    //TODO (eric): Can this be cleaned up?
    public bool IsValidSetupLocation( Location location )
    {
        bool output = true;

        if( location is Intersection
         && !location.HasPiece<Town>( ) )
        {
            //The passed location is an empty intersection
            Intersection intersectionLocation = location as Intersection;
            
            if( intersectionLocation.OnLand( ) )
            {
                List<Intersection> adjIntersections = intersectionLocation.GetAdjacentIntersections( );

                foreach( Intersection intersection in adjIntersections )
                {
                    if( intersection.HasPiece<Town>( ) )
                    {
                        //You cannot place a settlement adjacent to another
                        //settlement or city.
                        output = false;
                        break;
                    }
                }
            }
            else
            {
                //The intersection must have an adjacent land hex.
                output = false;
            }
        }
        else
        {
            //You can only place a settlement on an intersection without a town on it.
            output = false;
        }

        return output;
    }

    //converts terrainKind to resourceType
    public ResourceType converter( TerrainKind terrainKind )
    {
        switch( terrainKind )
        {
            case TerrainKind.Pasture:
                return ResourceType.Wool;
            case TerrainKind.Forest:
                return ResourceType.Lumber;
            case TerrainKind.Mountain:
                return ResourceType.Ore;
            case TerrainKind.Hill:
                return ResourceType.Brick;
            case TerrainKind.Field:
                return ResourceType.Grain;
            case TerrainKind.GoldMine:
                return ResourceType.Nothing;
            case TerrainKind.Lake:
                return ResourceType.Nothing;
            case TerrainKind.Sea:
                return ResourceType.Nothing;
            case TerrainKind.Desert:
                return ResourceType.Nothing;
            default:
                return ResourceType.Nothing;
        }
    }
}
