﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Piece : NetworkBehaviour
{
    public Interactable MainInteractable;
	public Interactable MoveToInteractable;

    protected virtual void Awake( )
    {
        //Pieces should be inactive until they are placed on the board.
        gameObject.SetActive( false );
        transform.parent = GameObject.Find( "Pieces" ).transform;
    }

    private Color _color;
    public Color Color
    {
        get
        {
            return _color;
        }
        set
        {
            _color = value;
            this.GetComponent<Renderer>( ).material.color = value;
        }
    }

    private PlayerController _owner;
    public PlayerController Owner
    {
        get
        {
            return _owner;
        }
        set
        {
            _owner = value;
            Color = _owner.Color;
        }
    }
    
    public Location Location
    {
        get;
        set;
    }

    public abstract Dictionary<Card,int> Cost
    {
        get;
    }

	// Check whether a piece can be placed at a specific location
    public abstract bool CanPlaceAtLocation( Location location );
	// Check whether a piece can be moved to a specific location
	public abstract bool CanMoveToLocation (Location location);
	// Check whether a piece can be moved
	public abstract bool CanMove ();

    public virtual SerializablePiece AsSerializable( )
    {
        SerializablePiece output = new SerializablePiece( );

        if( Location == null )
        {
            output._location = null;
        }
        else
        {
            output._location = Location.AsSerializable( );
        }

        output.playerId = PlayerController.ColorToId( Owner.Color );
        return output;
    }
}

[Serializable]
public class SerializablePiece
{
    public List<SerializableHex> _location;
    public int playerId;
}
