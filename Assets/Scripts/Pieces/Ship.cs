﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : Route
{
    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int> { { new ResourceCard( ResourceType.Wool ), 1 }
                                             , { new ResourceCard( ResourceType.Lumber ), 1 } };
        }
    }
		
    public override bool CanPlaceAtLocation( Location location )
    {
        bool connectedToRoute = false;
        bool adjacentToSea = false;

        // Ship can only be placed on Edges that do not already have a Ship or
        // Road on them
        if( location is Edge
         && !location.HasPiece<Route>( ) )
        {
            Edge edgeLocation = location as Edge;
            List<Edge> adjEdgeLocations = edgeLocation.GetAdjacentEdges( );

            //Edge must be next to either a ship or town owned by the same player.
            foreach( Edge edge in adjEdgeLocations )
            {
                if( edge.HasPieceOwnedByPlayer<Ship>( Owner ) )
                {
                    connectedToRoute = true;
                    break;
                }
            }

            foreach( Intersection intersection in edgeLocation.Intersections )
            {
                if( intersection.HasPieceOwnedByPlayer<Town>( Owner ) )
                {
                    connectedToRoute = true;
                    break;
                }
            }

            //Edge must be next to a sea hex.
			if (edgeLocation.InOcean ()) {
				adjacentToSea = true;
			}
        }

        return connectedToRoute && adjacentToSea;
    }

	public override bool CanMoveToLocation(Location location) {
		bool connectedToRoute = false;
		bool adjacentToSea = false;

		// Ship can only be placed on Edges that do not already have a Ship or
		// Road on them
		if( location is Edge
			&& !location.HasPiece<Route>( ) )
		{
			Edge edgeLocation = location as Edge;
			List<Edge> adjEdgeLocations = edgeLocation.GetAdjacentEdges( );

			//Edge must be next to either a ship or town owned by the same player.
			foreach( Edge edge in adjEdgeLocations )
			{
				if( edge.HasPieceOwnedByPlayer<Ship>( Owner ) && edge.GetPiece<Ship>() != this)
				{
					connectedToRoute = true;
					break;
				}
			}

			foreach( Intersection intersection in edgeLocation.Intersections )
			{
				if( intersection.HasPieceOwnedByPlayer<Town>( Owner ) )
				{
					connectedToRoute = true;
					break;
				}
			}

			//Edge must be next to a sea hex.
			if (edgeLocation.InOcean ()) {
				adjacentToSea = true;
			}
		}

		return connectedToRoute && adjacentToSea;
	}

	// checks if the ship can be moved
	public override bool CanMove() {
		Edge edgeLocation = this.Location as Edge;

		foreach (Intersection intersection in edgeLocation.Intersections) {
			if (!intersection.HasPieceOwnedByPlayer<Piece> (Owner)) {
				for (int i = 0; i < intersection.Edges.Count; i++) {
					if (intersection.Edges [i] == edgeLocation) {
						if (i == intersection.Edges.Count - 1) {
							return true;
						} 
					}
					else if (!intersection.Edges [i].HasPieceOwnedByPlayer<Piece> (Owner)) {
						if (i == intersection.Edges.Count - 1) {
							return true;
						} 
					}
					else {
						break;
					}
				}
			} 
		}

		return false;
	}

}
