﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrongKnight : Knight
{
    public override int Strength
    {
        get
        {
            return 2;
        }
    }

	public override bool CanPlaceAtLocation (Location location) {
		// if the location is an intersection and does not already have piece on it
		if (location.HasPieceOwnedByPlayer<BasicKnight> (Owner)) {
			return true;
		}
		return false;
	}
		
}
