﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Knight : IntersectionPiece
{
	public Interactable ActivateInteractable;

    public abstract int Strength
    {
        get;
    }

    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int>( ) { { new ResourceCard( ResourceType.Ore ), 1 }
                                                , { new ResourceCard( ResourceType.Wool ), 1 } };
        }
    }

	[SyncVar]
    protected bool Active = false; //active / inactive
    //private PlayerController Player; //the player this knight belongs too
    //private Intersection intersection; //the intersection this knight is on
    //                                   //renderer
    //                       //Turn Restrictions
    //private bool canMoved; //if knight has been activated this turn
    //private bool hasMoved; //if knight has moved this turn
	protected bool wasPromoted = false; //if knight was promoted this turn
    protected bool wasBuilt = false; // if the knight was built this turn
    
    //activate the knight
    public void Activate( )
    {
        Active = true;
    }

	//deactivate the knight
	public void Deactivate() {
		Active = false;
	}

    //public void Move( Intersection i )
    //{
    //    this.intersection = i;
    //    i.piece = this;
    //    hasMoved = true;
    //}

    //public void Promote( )
    //{
    //    this.Level += 1;
    //    hasPromoted = true;
    //}

	public override bool CanMoveToLocation(Location location) {
		// if the location is an intersection 
		if (location is Intersection) {
			Intersection intersectionLocation = location as Intersection;
			// if the intersection does not already have a piece or has an opponent knight with inferior strength
			if (!intersectionLocation.HasPiece<Piece> () || intersectionLocation.HasInferiorKnight(this)) {
				// if the location is connected by a route to the piece's current location
				if (intersectionLocation.ConnectedByRoute (this.Location as Intersection, Owner)) {
					return true;
				}
			}
		}
		return false;
	}

	public override bool CanMove() {
		if (!wasBuilt && Active) {
			return true;
		} 
		else {
			return false;
		}
	}

	public bool active {
		get {
			return active;
		}
	}

	public bool WasBuilt {
		get {
			return wasBuilt;
		}
		set {
			wasBuilt = value;
		}
	}

	public bool WasPromoted {
		get {
			return wasPromoted;
		}
		set {
			wasPromoted = value;
		}
    }

    public override SerializablePiece AsSerializable( )
    {
        SerializableKnight output = new SerializableKnight( );
        output._location = Location.AsSerializable( );
        output.playerId = PlayerController.ColorToId( Owner.Color );
        output.Active = Active;
        output.wasPromoted = wasPromoted;
        output.wasBuilt = wasBuilt;
        return output;
    }
}

[Serializable]
public class SerializableKnight : SerializablePiece
{
    public bool Active;
    public bool wasPromoted;
    public bool wasBuilt;
}