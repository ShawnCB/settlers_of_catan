﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Pirate : HexPiece {

	//not that Pirate is not activated until first barbarian attack
	[SyncVar]
	private bool activate = false;

	public override Dictionary<Card, int> Cost
	{
		get
		{
			//You cannot build a pirate, so its cost is free.
			return new Dictionary<Card, int>( );
		}
	}

	//makes the Pirate activated for usage after the first barbarian roll
	public void Activate(){
		activate = true;
	}

	//check activation for moving Pirate
	public bool isActivated(){
		return activate;
	}

	public override bool CanPlaceAtLocation( Location location )
	{
		bool output = false;

		if( (location is Hex) && (!location.HasPiece<Pirate>( )) )
		{
			Hex hexLocation = location as Hex;
			if (hexLocation.terrainKind == TerrainKind.Sea) {
				output = true;
			}
		}

		return output;
	}

	public override bool CanMoveToLocation(Location location) {
		bool output = false;

		if( (location is Hex) && (!location.HasPiece<Pirate>( )) )
		{
			Hex hexLocation = location as Hex;
			if (hexLocation.terrainKind == TerrainKind.Sea) {
				output = true;
			}
		}

		return output;
	}

	public override bool CanMove() {
		return true;
	}
}

