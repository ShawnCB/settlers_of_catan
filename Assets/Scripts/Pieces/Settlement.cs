﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settlement : Town
{
    public override Dictionary<Card, int> Cost
    {
        get
        {
            return new Dictionary<Card, int> { { new ResourceCard( ResourceType.Brick ), 1 }
                                             , { new ResourceCard( ResourceType.Lumber ), 1 }
                                             , { new ResourceCard( ResourceType.Wool ), 1 }
                                             , { new ResourceCard( ResourceType.Grain ), 1 } };
        }
    }

    //This method checks if a settlement can be placed on this intersection by the local player
    public override bool CanPlaceAtLocation( Location location )
    {
        bool output = true;

        if( location is Intersection
         && !location.HasPiece<Piece>( ) )
        {
            //The passed location is an empty intersection
            Intersection intersectionLocation = location as Intersection;

            //Check the adjacent intersections for towns (see Catan Distance Rule).
            List<Intersection> adjIntersections = intersectionLocation.GetAdjacentIntersections( );

            foreach( Intersection intersection in adjIntersections )
            {
                if( intersection.HasPiece<Town>( ) )
                {
                    //You cannot place a settlement adjacent to another
                    //settlement or city.
                    output = false;
                    break;
                }
            }

            //If output is true, then we obey the distance rule.
            //Check if this location is adjacent to a land hex, and
            //check the adjacent edges for a route we can attach to.
            if( output && intersectionLocation.OnLand( ) )
            {
                bool adjToRoute = false;

                foreach( Edge edge in intersectionLocation.Edges )
                {
                    if( edge.HasPiece<Route>( ) )
                    {
                        adjToRoute = true;
                        break;
                    }
                }

                output = adjToRoute;
            }
        }
        else
        {
            //You can only place a settlement on an empty intersection
            output = false;
        }

        return output;
    }

	public override bool CanMoveToLocation(Location location) {
		return false;
	}

	public override bool CanMove() {
		return false;
	}
}
