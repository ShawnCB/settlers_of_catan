﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MaritimeTrade : MonoBehaviour{

	//public GameController game; //the game
	//public PlayerController player; //the current player
	//list of card types to trade, with 4 card for 1 card
	//Dictionary<Card,Card> cards = new Dictionary<Card,Card>();
	// Use this for initialization (need to fit GUI)
	void Start () {
		//by GUI, tomorrow...
	}
	//missing an add to cards function for the player to trade from GUI

	//trading function
	public static void Trade(Dictionary<Card,Card> hand, GameController game, PlayerController player){
		foreach(KeyValuePair<Card,Card> exchange in hand){
			ResourceCard with = (ResourceCard) exchange.Key; //with 4 kind of this card type
			ResourceCard to = (ResourceCard) exchange.Value; //exchange to 1 kind of this card type
			ResourceType toExchange = with.type; //the resourcetype we have
			ResourceType toGet = to.type; //the resourcetype we want
			//return card we have to bank

			Dictionary<Card,int> stealables = player.StealableCards;

			int handAmount = -1;
			foreach (KeyValuePair<Card,int> entry in stealables) {
				if (entry.Key is ResourceCard) {
					if ((entry.Key as ResourceCard).type == with.type) {
						handAmount = entry.Value;
					}
				}
			}

			Dictionary<ResourceCard,int> deckResources = game.ResourceCards;
			int deckAmount = -1;
			foreach (KeyValuePair<ResourceCard,int> entry in deckResources) {
				if ((entry.Key as ResourceCard).type == with.type) {
					deckAmount = entry.Value;
				}
			}

			if(handAmount >= 4 && deckAmount >= 1){
				player.RemoveCards( new Dictionary<Card, int> { { new ResourceCard( toExchange ), 4 } } ); //reduce card we have by 4.
				player.AddCards( new Dictionary<Card, int> { { new ResourceCard( toGet ), 1 } } ); //increment card we want by 1.
				game.SetResourceText ();
			}
		}
	}

}
