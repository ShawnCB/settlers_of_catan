﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LakeHex : Hex {

	//dice roll numbers for LakeHex
	public int[] DiceNumbers = {2,3,11,12};
	//public TerrainKind terrainKind = TerrainKind.Lake;

	//change a Hex to lakeHex
	public void toLakeHex(Hex hex){
		if (hex.terrainKind == TerrainKind.Desert) {
			LakeHex lake = new LakeHex ();
			hex = lake;
		}
	}
}
