﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    protected GameController _owner;
    protected PlayerController _localPlayer;
    
    public Location Location
    {
        get;
        set;
    }

    public Piece PieceToPlace
    {
        get;
        set;
    }
		
	public Piece PieceToMove {
		get;
		set;
	}

    protected virtual void Awake( )
    {
        _owner = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );
        _localPlayer = _owner.GetLocalPlayer( );
        transform.parent = GameObject.Find( "Interactables" ).transform;
    }
}
