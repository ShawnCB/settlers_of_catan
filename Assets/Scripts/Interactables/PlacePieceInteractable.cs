﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacePieceInteractable : Interactable
{
    // when the interactable is clicked by the mouse
    void OnMouseDown( )
    {
		if (PieceToPlace is BasicKnight) {
			(PieceToPlace as Knight).WasBuilt = true;
		} 
		else if (PieceToPlace is City) {
			_localPlayer.CmdRemovePieceFromBoard (Location.GetPiece<Settlement> ().gameObject);
		} 
		else if (PieceToPlace is StrongKnight) {
			_localPlayer.CmdRemovePieceFromBoard (Location.GetPiece<BasicKnight> ().gameObject);
		}
		else if (PieceToPlace is MightyKnight) {
			_localPlayer.CmdRemovePieceFromBoard (Location.GetPiece<StrongKnight> ().gameObject);
		}

        // call the CmdBuildPiece method
        _localPlayer.CmdPlacePieceOnBoard( PieceToPlace.gameObject, Location.gameObject );

		(_owner.CurrentPhase as ActiveMainPhase).Action = false;
        _owner.DestroyInteractables( );
    }
}
