﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupOneTownInteractable : Interactable
{
    public SetupOneRoadInteractable roadInteractablePrefab;

    // when the interactable is clicked by the mouse
    void OnMouseDown( )
    {
        // Place the settlement at this location
        _localPlayer.CmdPlacePieceOnBoard( PieceToPlace.gameObject, Location.gameObject );

        // Make sure we clear all of the existing interactables before we place
        // the next set of them.
        _owner.DestroyInteractables( );

        // Place the interactables for the next part of this player's setup round one.
        Road road = _owner.GetLocalPlayer( ).PieceManager.GetFreePiece<Road>( );
        List<Edge> availableEdges = (Location as Intersection).Edges;

        foreach( Edge edge in availableEdges )
        {
            if( !edge.HasPiece<Route>( ) )
            {
                SetupOneRoadInteractable interactable = Instantiate( roadInteractablePrefab
																   , edge.transform.position
                                                                   , Quaternion.identity );
                interactable.PieceToPlace = road;
                interactable.Location = edge;
                _owner.AddInteractable( interactable );
            }
        }
    }
}
