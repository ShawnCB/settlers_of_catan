﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePieceSelectionInteractable : Interactable {

	// when the interactable is clicked by the mouse
	void OnMouseDown() {
		_owner.DestroyInteractables ();
		_owner.CreateMoveToInteractables (PieceToMove);
	}

}
