﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePieceToInteractable : Interactable {

	// when the interactable is clicked by the mouse
	void OnMouseDown() {
		StartCoroutine (HandleMouseClick ());
	}

	private IEnumerator HandleMouseClick() {
		// keep track of the original location of the piece we are moving
		// if the piece we are moving was displaced by another player, we can use this location to get the player
		Location originalLocation = PieceToMove.Location;

		_localPlayer.CmdRemovePieceFromBoard (PieceToMove.gameObject);
		_localPlayer.CmdPlacePieceOnBoard( PieceToMove.gameObject, Location.gameObject );

		if (PieceToMove is Ship) {
			PieceToMove.Owner.movedShip = true;
		} 
		else if (PieceToMove is Knight) {
			_localPlayer.CmdDeactivateKnight (PieceToMove.gameObject);

			Intersection intersectionLocation = Location as Intersection;
			Knight movingKnight = PieceToMove as Knight;
			// if we are displacing a knight
			if (intersectionLocation.HasInferiorKnight (movingKnight)) {
				Knight displacedKnight = intersectionLocation.GetPiece<Knight> ();
				PlayerController displacedOwner = displacedKnight.Owner;

				displacedKnight.gameObject.SetActive (false);
				// wait for coroutine to finish
				yield return DisplaceKnight (displacedOwner, displacedKnight);
			}
		}

		if (_owner.CurrentPhase is PassiveMainPhase) {
			originalLocation.GetPiece<Knight> ().Owner.CmdReceiveReply ();
		}

		if (_owner.CurrentPhase is ActiveMainPhase) {
			(_owner.CurrentPhase as ActiveMainPhase).Action = false;
		}
		_owner.DestroyInteractables( );
	}

	private IEnumerator DisplaceKnight(PlayerController displacedOwner, Knight displacedKnight) {
		// send a message to the player who had his knight displaced
		displacedOwner.CmdRespondToDisplaceKnight (displacedKnight.gameObject);

		// wait and do nothing while no reply received
		while (_owner.GetLocalPlayer ().receivedReply < 1) {
			yield return new WaitForSeconds (0.1f);
		}

		// set reply received back to 0
		_owner.GetLocalPlayer ().receivedReply = 0;
	}
}
