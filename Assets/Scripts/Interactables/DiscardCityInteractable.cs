﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardCityInteractable : Interactable {

	// when the interactable is clicked by the mouse
	void OnMouseDown() {
		_localPlayer.CmdRemovePieceFromBoard (Location.GetPiece<City> ().gameObject);
		_localPlayer.CmdPlacePieceOnBoard (_localPlayer.PieceManager.GetFreePiece<Settlement> ().gameObject, Location.gameObject);

		_owner.CmdReceiveReply ();
		_owner.DestroyInteractables ();
	}

}
