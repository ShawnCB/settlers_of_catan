﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupTwoTownInteractable : Interactable
{
    public SetupTwoRoadInteractable roadInteractablePrefab;

    // when the interactable is clicked by the mouse
    void OnMouseDown( )
    {
        // Place the settlement at this location
        _localPlayer.CmdPlacePieceOnBoard( PieceToPlace.gameObject, Location.gameObject );

        // Make sure we clear all of the existing interactables before we place
        // the next set of them.
        _owner.DestroyInteractables( );

		// Assign resources
		_owner.GetLocalPlayer().CmdRoundTwoAssignResources( PieceToPlace.gameObject );

        // Create the interactables for placing this player's second route.
        Road road = _owner.GetLocalPlayer( ).PieceManager.GetFreePiece<Road>( );
        List<Edge> availableEdges = (Location as Intersection).Edges;

        foreach( Edge edge in availableEdges )
        {
            if( !edge.HasPiece<Route>( ) )
            {
                SetupTwoRoadInteractable interactable = Instantiate( roadInteractablePrefab
																   , edge.transform.position
                                                                   , Quaternion.identity );
                interactable.PieceToPlace = road;
                interactable.Location = edge;
                _owner.AddInteractable( interactable );
            }
        }
    }
}
