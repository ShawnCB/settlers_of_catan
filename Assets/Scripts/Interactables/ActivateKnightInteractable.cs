﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateKnightInteractable : Interactable {

	// when the interactable is clicked by the mouse
	void OnMouseDown() {
		_localPlayer.CmdRemoveCards(new string[] {ResourceType.Grain.ToString()});
		_localPlayer.CmdActivateKnight (Location.GetPiece<Knight> ().gameObject);

		_owner.DestroyInteractables ();
	}

}
