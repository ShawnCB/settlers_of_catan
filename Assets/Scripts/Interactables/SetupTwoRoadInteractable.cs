﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupTwoRoadInteractable : Interactable
{
    public Edge location;

    // when the interactable is clicked by the mouse
    void OnMouseDown( )
    {
        // Place the piece at this location.
        // TODO (eric): We need to actually check if this is a land-only, coastal, or sea-only
        //              location to decide what piece to buid. If it is a coastal location, we need
        //              to create an interface for the user to select whether they want a road or a
        //              ship.
        _localPlayer.CmdPlacePieceOnBoard( PieceToPlace.gameObject, Location.gameObject );
        _owner.DestroyInteractables( );

        // we have to set the setupOneCompleted attribute of the player to true
        // we also have to increment the active player index and check state on all machines
        _localPlayer.CmdSetSetupTwoDone( );
    }
}
