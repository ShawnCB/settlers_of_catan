﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    // reference to the game
    private GameController _gameController = null;
    private GameController GameController
    {
        get
        {
            //Note: We used to find the GameController object in
            //      PlayerController::Start. However, clients can create
            //      PlayerController objects before the GameController object
            //      has been created, causing errors. Therefore, we simply
            //      delay retrieval of the GameController object until it is
            //      first required.
            //      
            //      This can potentially return null if called before the
            //      GameController has been constructed, so take care in when
            //      you ask for it.
            if( _gameController == null )
            {
                //Expects us to only ever have at most one GameController in the scene.
                GameObject gameControllerObj = GameObject.FindWithTag( "GameController" );
                if( gameControllerObj != null )
                {
                    _gameController = gameControllerObj.GetComponent<GameController>( );
                }
            }
            return _gameController;
        }
        set
        {
            _gameController = value;
        }
    }

    // Pieces the player owns
    private PieceManager _pieceManager = new PieceManager( );
    public PieceManager PieceManager
    {
        get
        {
            return _pieceManager;
        }
        private set
        {
            _pieceManager = PieceManager;
        }
    }

    //returns all stealable cards
    public Dictionary<Card, int> _stealableCards = new Dictionary<Card, int>( );
    public Dictionary<Card, int> StealableCards
    {
        get
        {
            return _stealableCards;
        }
        set
        {
            _stealableCards = value;
        }
    }

    //returns all progress cards
    public Dictionary<ProgressCard, int> _progressCards = new Dictionary<ProgressCard, int>( );
    public Dictionary<ProgressCard, int> ProgressCards
    {
        get
        {
            return _progressCards;
        }
        set
        {
            _progressCards = value;
        }
    }

    //private colour of the player
    public Color Color
    {
        get;
        set;
    }

    private int VictoryPoints = 0;
    public void AddVp()
    {
        VictoryPoints++;
        DebugConsole.Log("You gain a Victory Point!");
        if (VictoryPoints >= 10)
        {
            DebugConsole.Log("you win!");
        }
    }
    public int _colorId = -1;

    //This is bad workaround to store the player color when serializing.
    //TODO (eric): If we miraculously find time, see about a better way of storing players.
    public static int ColorToId( Color color )
    {
        int output = -1;

        if( color == Color.red )
        {
            output = 0;
        }
        else if( color == Color.white )
        {
            output = 1;
        }
        else if( color == Color.yellow )
        {
            output = 2;
        }
        else if( color == Color.blue )
        {
            output = 3;
        }

        return output;
    }

    public static Color IdToColor( int colorId )
    {
        Color color;

        switch( colorId )
        {
            case 0:
                color = Color.red;
                break;
            case 1:
                color = Color.white;
                break;
            case 2:
                color = Color.yellow;
                break;
            case 3:
                color = Color.blue;
                break;
            default:
                //Use a bad color?
                color = Color.black;
                break;
        }

        return color;
    }
    
	private bool _movedShip = false;
	public bool movedShip {
		get {
			return _movedShip;
		}
		set {
			_movedShip = value;
		}
	}

	private int _receivedReply = 0;
	public int receivedReply {
		get {
			return _receivedReply;
		}
		set {
			_receivedReply = value;
		}
	}

    //TODO (eric): Merge these into a single boolean that is reused between phases
    public bool lobbyPhaseReady = false;
    public bool setupOneCompleted = false;
    public bool setupTwoCompleted = false;
    
    [SyncVar]
    public int defenderOfCatanVps;

    // Use this for initialization
    void Awake( )
    {
        initCards( );
    }

    // Use this for initialization
    void Start( )
    {
        GameController.AddConnectedPlayer( this );
        defenderOfCatanVps = 0;
    }

    [Command]
    public void CmdSetPlayerColor( int colorId )
    {
        RpcSetPlayerColor( colorId );
    }

    [ClientRpc]
    public void RpcSetPlayerColor( int colorId )
    {
        _colorId = colorId;
        Color = IdToColor( colorId );
    }

    [Command]
    public void CmdRemovePieceFromBoard( GameObject pieceObject )
    {
        GameController.RpcRemovePieceFromBoard( pieceObject );
    }

    [Command]
    public void CmdPlacePieceOnBoard( GameObject pieceObject, GameObject locationObject )
    {
        GameController.RpcPlacePieceOnBoard( pieceObject, locationObject );
    }

	[Command]
	public void CmdActivateKnight (GameObject pieceObject) {
		Knight knight = pieceObject.GetComponent<Knight> ();

		if (knight != null) {
			knight.Activate ();
		} 
		else {
			DebugConsole.Log( "GameObject without Knight component passed to PlayerController::CmdActivateKnight", "error" );
		}
	}

	[Command]
	public void CmdDeactivateKnight (GameObject pieceObject) {
		Knight knight = pieceObject.GetComponent<Knight> ();

		if (knight != null) {
			knight.Deactivate ();
		} 
		else {
			DebugConsole.Log( "GameObject without Knight component passed to PlayerController::CmdDeactivateKnight", "error" );
		}
	}

	[Command]
	public void CmdActivateRobber (GameObject pieceObject) {
		Robber robber = pieceObject.GetComponent<Robber> ();

		if (robber != null) {
			robber.Activate ();
		} 
		else {
			DebugConsole.Log( "GameObject without Robber component passed to PlayerController::CmdDeactivateRobber", "error" );
		}
	}

	[Command]
	public void CmdActivatePirate (GameObject pieceObject) {
		Pirate pirate = pieceObject.GetComponent<Pirate> ();

		if (pirate != null) {
			pirate.Activate ();
		} 
		else {
			DebugConsole.Log( "GameObject without Robber component passed to PlayerController::CmdDeactivateRobber", "error" );
		}
	}

    [Command]
    public void CmdStartTrade( int a, int b )
    {
        RpcStartTrade( a, b );
    }

    [ClientRpc]
    public void RpcStartTrade( int a, int b )
    {
        GameController.startTrade( a, b, this );
    }

    [Command]
    public void CmdRoundTwoAssignResources( GameObject pieceObject )
    {
        GameController.RpcRoundTwoAssignResources( pieceObject );
    }

    [Command]
    public void CmdSetSetupOneDone( )
    {
        RpcSetSetupOneDone( );
    }

    //"this" player object finished their setup one phase.
    [ClientRpc]
    public void RpcSetSetupOneDone( )
    {
        setupOneCompleted = true;

        if( GameController.AllPlayersSetupOneCompleted( ) )
        {
            GameController.ChangePhase( );
        }
        else
        {
            //Move to the next player
            GameController.EndTurn( );
        }
    }

    [Command]
    public void CmdSetSetupTwoDone( )
    {
        RpcSetSetupTwoDone( );
    }

    // Inform the server that a client has ended their turn.
    // TODO: Pass the PlayerController who is ending their turn, 
    //       to verify it is in fact their turn to end.
    [Command]
    public void CmdEndTurn( )
    {
        GameController.RpcEndTurn( );
    }

    //"this" player object finished their setup two phase.
    [ClientRpc]
    public void RpcSetSetupTwoDone( )
    {
        setupTwoCompleted = true;

        if( GameController.AllPlayersSetupTwoCompleted( ) )
        {
            GameController.ChangePhase( );
        }
        else
        {
            //Move to the next player
            GameController.EndTurn( );
        }
    }

    [Command]
    public void CmdRollDice( )
    {
        //DebugConsole.Log("dice roll?");
        GameController.RollDice( );
    }

    [ClientRpc]
    public void RpcAssignPiece( GameObject pieceObject )
    {
        Piece piece = pieceObject.GetComponent<Piece>( );
        piece.Owner = this;
        PieceManager.AddPiece( piece );
    }

	[Command]
	public void CmdRespondToDisplaceKnight(GameObject pieceObject) {
		RpcRespondToDisplaceKnight (pieceObject);
	}

	[ClientRpc]
	public void RpcRespondToDisplaceKnight(GameObject pieceObject) {
		if (this.isLocalPlayer) {
			Knight displacedKnight = pieceObject.GetComponent<Knight> ();

			if (displacedKnight != null) {
				GameController.CreateMoveToInteractables (displacedKnight);
			} 
			else {
				DebugConsole.Log ("GameObject without Knight component passed to PlayerController::RpcRespondToDisplaceKnight", "error");
			}
		} 
		else {
			DebugConsole.Log ("Waiting for " + this.name + " action");
		}
	}

	[Command]
	public void CmdRespondToDiscardCards(int numCards) {
		RpcRespondToDiscardCards (numCards);
	}

	[ClientRpc]
	public void RpcRespondToDiscardCards(int numCards) {
		if (this.isLocalPlayer) {
			// TODO: call the method that will allow player to choose the cards to discard through UI

		}
	}

	[ClientRpc]
	public void RpcRespondToDiscardCity() {
		if (this.isLocalPlayer && PieceManager.GetPiecesOnBoard<City>().Count > 0) {
			GameController.CreateDiscardCityInteractables();
		}
	}
		
	[ClientRpc]
	public void RpcRespondToDrawProgressCard() {
		if (this.isLocalPlayer) {
			// call the method that will allow player to choose progress card through UI
			GameController.uiManager._mainPhaseUI._chooseProgressCardPanel.SetActive(true);
		}
	}

	[ClientRpc]
	public void RpcRespondToMoveRobber() {
		if (this.isLocalPlayer) {
			// call the method that allows player to move robber/pirate and steal resources from other players
			StartCoroutine(_gameController.RespondToMoveRobber());
		}
	}

	[Command]
	public void CmdReceiveReply() {
		RpcReceiveReply ();
	}

	[ClientRpc]
	public void RpcReceiveReply() {
		if (this.isLocalPlayer) {
			_receivedReply++;
		}
	}

	[ClientRpc]
	public void RpcSetUI() {
		if (this.isLocalPlayer) {
			GameController.uiManager._mainPhaseUI._actionButton.gameObject.SetActive(true);
			GameController.uiManager._mainPhaseUI._actionPanel.gameObject.SetActive (false);
			GameController.uiManager._mainPhaseUI._endTurnButton.gameObject.SetActive (true);
			GameController.uiManager._mainPhaseUI._rollDiceButton.gameObject.SetActive (false);
		}
	}

    //initialize the cards in a player's hand
    public void initCards( )
    {
        StealableCards.Add( new ResourceCard( ResourceType.Brick ), 0 );
        StealableCards.Add( new ResourceCard( ResourceType.Grain ), 0 );
        StealableCards.Add( new ResourceCard( ResourceType.Lumber ), 0 );
        StealableCards.Add( new ResourceCard( ResourceType.Ore ), 0 );
        StealableCards.Add( new ResourceCard( ResourceType.Wool ), 0 );

        StealableCards.Add( new CommodityCard( CommodityType.Coin ), 0 );
        StealableCards.Add( new CommodityCard( CommodityType.Paper ), 0 );
        StealableCards.Add( new CommodityCard( CommodityType.Cloth ), 0 );

        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.CommercialHarbor ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.MasterMerchant ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.Merchant ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.MerchantFleet ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.ResouceMonopoly ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.TradeMonopoly ), 0 );

        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Bishop ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Constitution ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Deserter ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Diplomat ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Intrigue ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Saboteur ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Spy ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Warlord ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Wedding ), 0 );

        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Alchemist ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Crane ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Engineer ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Inventor ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Irrigation ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Medicine ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Mining ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Printer ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.RoadBuilding ), 0 );
        ProgressCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Smith ), 0 );
    }

    //change the number of cards in hand
    public void AddCards( Dictionary<Card, int> cards )
    {
        foreach( KeyValuePair<Card, int> entry in cards )
        {
            if( entry.Key is ProgressCard )
            {
                ProgressCards[entry.Key as ProgressCard] += entry.Value;
            }
            else
            {
                StealableCards[entry.Key] += entry.Value;
            }
        }

        GameController.RemoveCards( cards );
        GameController.SetResourceText( );
    }

    [Command]
    public void CmdAddCards( String[] cards )
    {
        RpcAddCards( cards );
    }

    //change the number of cards in hand
    [ClientRpc]
    public void RpcAddCards( String[] cards )
    {
        foreach( String cardName in cards )
        {
            //We cannot enumerate over a dictionary and modify its values at
            //the same time. To work around this, we create a list of the
            //dictionary's Keys, enumerate over that, and modify the value when
            //we find a match.
            List<Card> stealableCardTypes = new List<Card>( StealableCards.Keys );

            foreach( Card stealableCardType in stealableCardTypes )
            {
                if( (stealableCardType is ResourceCard)
                 && ((stealableCardType as ResourceCard).type.ToString( ) == cardName) )
                {
                    //Increment the resource type in the dictionary.
                    StealableCards[stealableCardType]++;
                    continue;
                }
                else if( (stealableCardType is CommodityCard)
                      && (stealableCardType as CommodityCard).type.ToString( ) == cardName )
                {
                    StealableCards[stealableCardType]++;
                    continue;
                }
            }

            List<ProgressCard> progressCardTypes = new List<ProgressCard>( ProgressCards.Keys );

            foreach( ProgressCard progressCardType in progressCardTypes )
            {
                if( (progressCardType as ProgressCard).type.ToString( ) == cardName )
                {
                    ProgressCards[progressCardType]++;
                }
            }

        }

        GameController.RemoveCards( cards );
        GameController.SetResourceText( );
    }

    //change the number of cards in hand
    public void RemoveCards( Dictionary<Card, int> cards )
    {
        foreach( KeyValuePair<Card, int> entry in cards )
        {
            if( entry.Key is ProgressCard )
            {
                ProgressCards[entry.Key as ProgressCard] -= entry.Value;
            }
            else
            {
                StealableCards[entry.Key] -= entry.Value;
            }
        }

        GameController.AddCards( cards );
        GameController.SetResourceText( );
    }

    [Command]
    public void CmdRemoveCards( String[] cards )
    {
        RpcRemoveCards( cards );
    }

    //change the number of cards in hand
    [ClientRpc]
    public void RpcRemoveCards( String[] cards )
    {
        foreach( String cardName in cards )
        {
            //We cannot enumerate over a dictionary and modify its values at
            //the same time. To work around this, we create a list of the
            //dictionary's Keys, enumerate over that, and modify the value when
            //we find a match.
            List<Card> stealableCardTypes = new List<Card>( StealableCards.Keys );

            foreach( Card stealableCardType in stealableCardTypes )
            {
                if( (stealableCardType is ResourceCard)
                 && ((stealableCardType as ResourceCard).type.ToString( ) == cardName) )
                {
                    //Increment the resource type in the dictionary.
                    StealableCards[stealableCardType]--;
                    continue;
                }
                else if( (stealableCardType is CommodityCard)
                      && (stealableCardType as CommodityCard).type.ToString( ) == cardName )
                {
                    StealableCards[stealableCardType]--;
                    continue;
                }
            }

            List<ProgressCard> progressCardTypes = new List<ProgressCard>( ProgressCards.Keys );

            foreach( ProgressCard progressCardType in progressCardTypes )
            {
                if( (progressCardType as ProgressCard).type.ToString( ) == cardName )
                {
                    ProgressCards[progressCardType]--;
                }
            }

        }

        GameController.AddCards( cards );
        GameController.SetResourceText( );
    }

    //get number of certain card
    public int getAmountInHand( ResourceType t )
    {
        foreach( KeyValuePair<Card, int> pair in StealableCards )
        {
            if( (pair.Key is ResourceCard)
             && (pair.Key as ResourceCard).type == t )
            {
                return pair.Value;
            }
        }
        return 0;   //shouldn't be called
    }

    public bool HasEnoughStealables( Dictionary<Card, int> request )
    {
        // Assume we have enough resources.
        bool output = true;

        foreach( KeyValuePair<Card, int> requestedAmount in request )
        {
            if( _stealableCards.ContainsKey( requestedAmount.Key )
             && _stealableCards[requestedAmount.Key] < requestedAmount.Value )
            {
                output = false;
                break;
            }
        }

        return output;
    }

	// Determines if the player can still move a piece of the given type (number of moves limited per turn)
	public bool CanMovePiece<T>() where T : Piece {
		// if the piece is a ship
		if (typeof(T) == typeof(Ship)) {
			if (!_movedShip) {
				return true;
			}
		}

		// if the piece is a knight
		if (typeof(T) == typeof(Knight)) {
			return true;
		}

		return false;
	}

	// Returns a list of pieces of a specific type that are movable
	public List<T> GetMovablePieces<T>() where T : Piece {
		List<T> output = new List<T> ();

		List<T> pieces = _pieceManager.GetPiecesOnBoard<T> ();

		foreach (T piece in pieces) {
			if (piece.CanMove ()) {
				output.Add (piece);
			}
		}

		return output;
    }

    public int GetNumStealables( )
    {
        int numStealables = 0;

        foreach( KeyValuePair<Card, int> entry in StealableCards )
        {
            if( entry.Key is ResourceCard )
            {
                numStealables += entry.Value;
            }
            else if( entry.Key is CommodityCard )
            {
                numStealables += entry.Value;
            }
        }

        return numStealables;
    }

    public SerializablePlayer AsSerializable( )
    {
        SerializablePlayer output = new SerializablePlayer( );

        output.colorId = PlayerController.ColorToId( Color );
        output._movedShip = movedShip;
        output._receivedReply = receivedReply;
        output._stealableCards = _stealableCards;
        output._progressCards = _progressCards;

        output._settlements = new List<SerializablePiece>( );
        List<Settlement> settlements = PieceManager.GetPieces<Settlement>( );
        foreach( Settlement settlement in settlements )
        {
            output._settlements.Add( settlement.AsSerializable( ) );
        }

        output._cities = new List<SerializablePiece>( );
        List<City> cities = PieceManager.GetPieces<City>( );
        foreach( City city in cities )
        {
            output._cities.Add( city.AsSerializable( ) );
        }

        output._roads = new List<SerializablePiece>( );
        List<Road> roads = PieceManager.GetPieces<Road>( );
        foreach( Road road in roads )
        {
            output._roads.Add( road.AsSerializable( ) );
        }

        output._ships = new List<SerializablePiece>( );
        List<Ship> ships = PieceManager.GetPieces<Ship>( );
        foreach( Ship ship in ships )
        {
            output._ships.Add( ship.AsSerializable( ) );
        }

        output._basicKnights = new List<SerializablePiece>( );
        List<BasicKnight> basicKnights = PieceManager.GetPieces<BasicKnight>( );
        foreach( BasicKnight knight in basicKnights )
        {
            output._basicKnights.Add( knight.AsSerializable( ) );
        }

        output._strongKnights = new List<SerializablePiece>( );
        List<StrongKnight> strongKnights = PieceManager.GetPieces<StrongKnight>( );
        foreach( StrongKnight knight in strongKnights )
        {
            output._strongKnights.Add( knight.AsSerializable( ) );
        }

        output._mightyKnights = new List<SerializablePiece>( );
        List<MightyKnight> mightyKnights = PieceManager.GetPieces<MightyKnight>( );
        foreach( MightyKnight knight in mightyKnights )
        {
            output._mightyKnights.Add( knight.AsSerializable( ) );
        }

        return output;
    }
}

[Serializable]
public class SerializablePlayer
{
    //Temporary.
    //0 is red, 1 is white, 2 is yellow, 3 is blue
    public int colorId;
    public bool _movedShip;
    public int _receivedReply;
    //Assume we cannot save a game during setup. Set those to true.
    //TODO: Remember the player's hand of cards.

    //Cards
    public Dictionary<Card, int> _stealableCards;
    public Dictionary<ProgressCard, int> _progressCards;

    //Pieces
    public List<SerializablePiece> _settlements;
    public List<SerializablePiece> _cities;
    public List<SerializablePiece> _roads;
    public List<SerializablePiece> _ships;

    //Note: These are really SerializableKnights
    public List<SerializablePiece> _basicKnights;
    public List<SerializablePiece> _strongKnights;
    public List<SerializablePiece> _mightyKnights;
}