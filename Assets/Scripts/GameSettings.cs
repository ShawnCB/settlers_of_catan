﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings
{
    private int _victoryPointsToWin = 13;
    public int VictoryPointsToWin
    {
        get
        {
            return _victoryPointsToWin;
        }
        set
        {
            _victoryPointsToWin = value;
        }
    }

    private int _settlementsPerPlayer = 5;
    public int SettlementsPerPlayer
    {
        get
        {
            return _settlementsPerPlayer;
        }
        set
        {
            _settlementsPerPlayer = value;
        }
    }

    private int _citiesPerPlayer = 4;
    public int CitiesPerPlayer
    {
        get
        {
            return _citiesPerPlayer;
        }
        set
        {
            _citiesPerPlayer = value;
        }
    }

    private int _roadsPerPlayer = 15;
    public int RoadsPerPlayer
    {
        get
        {
            return _roadsPerPlayer;
        }
        set
        {
            _roadsPerPlayer = value;
        }
    }

    private int _shipsPerPlayer = 15;
    public int ShipsPerPlayer
    {
        get
        {
            return _shipsPerPlayer;
        }
        set
        {
            _shipsPerPlayer = value;
        }
    }

	private int _basicKnightsPerPlayer = 2;
	public int BasicKnightsPerPlayer {
		get {
			return _basicKnightsPerPlayer;
		}
		set {
			_basicKnightsPerPlayer = value;
		}
	}

	private int _strongKnightsPerPlayer = 2;
	public int StrongKnightsPerPlayer {
		get {
			return _strongKnightsPerPlayer;
		}
		set {
			_strongKnightsPerPlayer = value;
		}
	}

	private int _mightyKnightsPerPlayer = 2;
	public int MightyKnightsPerPlayer {
		get {
			return _mightyKnightsPerPlayer;
		}
		set {
			_mightyKnightsPerPlayer = value;
		}
	}
}
