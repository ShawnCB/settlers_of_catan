﻿using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;

public class GameController : NetworkBehaviour
{
    public SetupOneTownInteractable setupOneTownInteractablePrefab;
    public SetupTwoTownInteractable setupTwoTownInteractablePrefab;
    public PlacePieceInteractable placePieceInteractablePrefab;
	public MovePieceSelectionInteractable movePieceSelectionInteractablePrefab;
    public Settlement _settlementPrefab;
    public City _cityPrefab;
    public Road _roadPrefab;
    public Ship _shipPrefab;
	public BasicKnight _basicKnightPrefab;
	public StrongKnight _strongKnightPrefab;
	public MightyKnight _mightyKnightPrefab;
	public Robber _robberPrefab;
	public Pirate _piratePrefab;
    public Die _diePrefab;
    public EventDie _eventDiePrefab;

    // Note: Use the property to change _currentPhase to automatically call the
    //       OnPhaseEnd and OnPhaseStart methods.
    private Phase _currentPhase;
	public Phase CurrentPhase
    {
        get
        {
            return _currentPhase;
        }

        private set
        {
            if( _currentPhase != null )
            {
                _currentPhase.OnPhaseEnd( );
            }

            _currentPhase = value;

            if( _currentPhase != null )
            {
                _currentPhase.OnPhaseStart( );
            }
        }
    }

    public GameSettings GameSettings
    {
        get;
        set;
    }

    // reference to the board gameobject
    private BoardController _boardController;
    public BoardController BoardController
    {
        get
        {
            if( _boardController == null )
            {
                GameObject boardControllerObj = GameObject.FindWithTag( "BoardController" );

                if( boardControllerObj != null )
                {
                    _boardController = boardControllerObj.GetComponent<BoardController>( );
                }
            }

            return _boardController;
        }

        private set
        {
            _boardController = value;
        }
    }
		
	// pieces that don't belong to any players (robber and pirate)
	private PieceManager pieceManager = new PieceManager ();

    private UIManager _uiManager;
	public UIManager uiManager {
		get {
			return _uiManager;
		}
	}

    public List<PlayerController> players = new List<PlayerController>( );

    private int activePlayerIndex = 0;

    private List<Interactable> currentInteractables = new List<Interactable>( );

    //Dictionary for cards
    public Dictionary<ResourceCard, int> ResourceCards = new Dictionary<ResourceCard, int>( );
    private Dictionary<CommodityCard, int> CommodityCards = new Dictionary<CommodityCard, int>( );
    private Dictionary<ProgressCard, int> TradeCards = new Dictionary<ProgressCard, int>( );
    private Dictionary<ProgressCard, int> PoliticsCards = new Dictionary<ProgressCard, int>( );
    private Dictionary<ProgressCard, int> ScienceCards = new Dictionary<ProgressCard, int>( );

    //Die objects which hold their last roll.
    //TODO (eric): On non-host clients, these references need to be updated when the dice
    //             GameObjects spawn. So they are public and there is an ugly registration
    //             in Die.Start( );
    public Die _redDie;
    public Die _yellowDie;
    public EventDie _eventDie;

    private const int BARBARIAN_STARTING_DISTANCE = 7;
    [SyncVar]
    private int _barbarianPosition = BARBARIAN_STARTING_DISTANCE;

    private Text resourceText;
    private Text diceText;
    private Text colourText;

    public int defenderOfCatanVpsAvailable = 6;

    public SerializableGame _loadedGame = null;

    // Use this for initialization
    void Awake( )
    {
        DebugConsole.Log( "GameController::Awake " + netId );
    }

    // Use this for initialization
    void Start( )
    {
        DebugConsole.Log( "GameController::Start " + netId );
        GameSettings = new GameSettings( );
        CurrentPhase = new LobbyPhase( );
        resourceText = GameObject.Find( "GUI/Resource Text" ).GetComponent<Text>( );
        diceText = GameObject.Find( "GUI/Dice Text" ).GetComponent<Text>( );
        colourText = GameObject.Find( "GUI/Colour Text" ).GetComponent<Text>( );
        _uiManager = GameObject.Find( "UIManager" ).GetComponent<UIManager>( );
        _loadedGame = null;
        CreateCards( );
    }

    // Draw state-specific GUI
    private void OnGUI( )
    {
        CurrentPhase.DrawGUI( );
    }

    [Server]
    public void SpawnGameObjects( )
    {
        GameObject yellowDieObject = Instantiate( _diePrefab.gameObject );
        _yellowDie = yellowDieObject.GetComponent<Die>( );
        _yellowDie.Identifier = "Yellow";
        NetworkServer.Spawn( yellowDieObject );

        GameObject redDieObject = Instantiate( _diePrefab.gameObject );
        _redDie = redDieObject.GetComponent<Die>( );
        _redDie.Identifier = "Red";
        NetworkServer.Spawn( redDieObject );

        GameObject eventDieObject = Instantiate( _eventDiePrefab.gameObject );
        _eventDie = eventDieObject.GetComponent<EventDie>( );
        _eventDie.Identifier = "Event";
        NetworkServer.Spawn( eventDieObject );

		GameObject robberObject = Instantiate (_robberPrefab.gameObject);
		robberObject.SetActive (false);
		NetworkServer.Spawn (robberObject);
		RpcAssignPiece (robberObject);

		GameObject pirateObject = Instantiate (_piratePrefab.gameObject);
		pirateObject.SetActive (false);
		NetworkServer.Spawn (pirateObject);
		RpcAssignPiece (pirateObject);

        foreach( PlayerController player in players )
        {
            //TODO (eric): Perhaps GameSettings can store a dictionary of
            //             GameObjects (representing the prefabs) to
            //             ints (representing the amount to spawn per player).

            //We want to spawn the pieces as inactive, so that they do not appear

            for( int i = 0; i < GameSettings.SettlementsPerPlayer; i++ )
            {
                GameObject newSettlement = Instantiate( _settlementPrefab.gameObject );
				newSettlement.SetActive (false);
                NetworkServer.Spawn( newSettlement );
                player.RpcAssignPiece( newSettlement );
            }

            for( int i = 0; i < GameSettings.CitiesPerPlayer; i++ )
            {
                GameObject newCity = Instantiate( _cityPrefab.gameObject );
				newCity.SetActive (false);
                NetworkServer.Spawn( newCity );
                player.RpcAssignPiece( newCity );
            }

            for( int i = 0; i < GameSettings.RoadsPerPlayer; i++ )
            {
                GameObject newRoad = Instantiate( _roadPrefab.gameObject );
				newRoad.SetActive (false);
                NetworkServer.Spawn( newRoad );
                player.RpcAssignPiece( newRoad );
            }

            for( int i = 0; i < GameSettings.ShipsPerPlayer; i++ )
            {
                GameObject newShip = Instantiate( _shipPrefab.gameObject );
				newShip.SetActive (false);
                NetworkServer.Spawn( newShip );
                player.RpcAssignPiece( newShip );
            }

			for( int i = 0; i < GameSettings.BasicKnightsPerPlayer; i++ )
			{
				GameObject newKnight = Instantiate( _basicKnightPrefab.gameObject );
				newKnight.SetActive (false);
				NetworkServer.Spawn( newKnight );
				player.RpcAssignPiece( newKnight );
			}

			for( int i = 0; i < GameSettings.StrongKnightsPerPlayer; i++ )
			{
				GameObject newKnight = Instantiate( _strongKnightPrefab.gameObject );
				newKnight.SetActive (false);
				NetworkServer.Spawn( newKnight );
				player.RpcAssignPiece( newKnight );
			}

			for( int i = 0; i < GameSettings.MightyKnightsPerPlayer; i++ )
			{
				GameObject newKnight = Instantiate( _mightyKnightPrefab.gameObject );
				newKnight.SetActive (false);
				NetworkServer.Spawn( newKnight );
				player.RpcAssignPiece( newKnight );
			}
        }
    }

    [ClientRpc]
    public void RpcSetPlayerOrder( int[] playerOrder )
    {
        List<PlayerController> newOrder = new List<PlayerController>( );

        for( int i = 0; i < playerOrder.Length; i++ )
        {
            for( int j = 0; j < players.Count; j++ )
            {
                if( players[j].netId.Value == playerOrder[i] )
                {
                    newOrder.Add( players[j] );
                    break;
                }
            }
        }

        activePlayerIndex = 0;
        players = newOrder;
        _uiManager.RegisterPlayers( players );
        SetColourText( );
    }

    [ClientRpc]
    public void RpcStartSetupOnePhase( )
    {
        ChangePhase( );
    }

    // Change the active player index and the phase on all clients.
    [ClientRpc]
    public void RpcEndTurn( )
    {
        EndTurn( );
    }

    [ClientRpc]
    public void RpcRemovePieceFromBoard( GameObject pieceObject )
    {
        Piece piece = pieceObject.GetComponent<Piece>( );

        if( piece != null )
        {
            piece.Location.RemovePiece( piece );
            piece.Location = null;
            piece.gameObject.SetActive( false );
        }
        else
        {
            DebugConsole.Log( "GameObject without Piece component passed to GameController::RpcRemovePieceFromBoard", "error" );
        }
    }

    [ClientRpc]
    public void RpcPlacePieceOnBoard( GameObject pieceObject, GameObject locationObject )
    {
        Piece piece = pieceObject.GetComponent<Piece>( );
        Location location = locationObject.GetComponent<Location>( );

        if( piece != null )
        {
            if( location != null )
            {
                piece.Location = location;
                location.AddPiece( piece );
                piece.gameObject.transform.position = location.gameObject.transform.position + new Vector3( 0, 0, -0.1f );
                piece.gameObject.transform.eulerAngles = location.gameObject.transform.eulerAngles;
                piece.gameObject.SetActive( true );
            }
            else
            {
                DebugConsole.Log( "GameObject without Piece component passed to GameController::RpcRemovePieceFromBoard", "error" );
            }
        }
        else
        {
            DebugConsole.Log( "GameObject without Piece component passed to GameController::RpcRemovePieceFromBoard", "error" );
        }
    }

	[ClientRpc]
	private void RpcAssignPiece(GameObject pieceObject) {
		Piece piece = pieceObject.GetComponent<Piece> ();
		pieceManager.AddPiece (piece);
	}

    public void EndTurn( )
    {
        //Special consideration for the second phase of setup, where turns are
        //in reverse order.
        if( CurrentPhase is SetupTwoPhase )
        {
            activePlayerIndex = (activePlayerIndex - 1) % players.Count;
        }
        else
        {
            activePlayerIndex = (activePlayerIndex + 1) % players.Count;
        }

        ChangePhase( );
    }

    public void ChangePhase( )
    {
        CurrentPhase = CurrentPhase.GetNextPhase( );
    }

    public static List<PlayerController> GetConnectedPlayers( )
    {
        List<PlayerController> output = new List<PlayerController>( );
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag( "Player" );

        foreach( GameObject gameObject in gameObjects )
        {
            output.Add( gameObject.GetComponent<PlayerController>( ) );
        }

        return output;
    }

    public bool AllPlayersSelectedColor( )
    {
        bool output = true;
        List<int> colorIds = new List<int>( );

        foreach( PlayerController player in players )
        {
            if( player._colorId == -1 )
            {
                output = false;
                DebugConsole.Log( "Attempted to start the game, but a player has not selected a color", "error" );
                break;
            }

            if( colorIds.Contains( player._colorId ) )
            {
                output = false;
                DebugConsole.Log( "Attempted to start the game, but more than one player has selected the same color", "error" );
                break;
            }

            colorIds.Add( player._colorId );
        }

        return output;
    }

    // Locate the PlayerController object that represents this client's player
    // Returns null if it cannot be found.
    public PlayerController GetLocalPlayer( )
    {
        foreach( PlayerController player in players )
        {
            if( player.isLocalPlayer )
            {
                return player;
            }
        }
        return null;
    }

    public bool IsActivePlayer( PlayerController player )
    {
        return players[activePlayerIndex].Equals( player );
    }

    public bool AllPlayersSetupOneCompleted( )
    {
        foreach( PlayerController player in players )
        {
            if( !player.setupOneCompleted )
            {
                return false;
            }
        }
        return true;
    }

    public bool AllPlayersSetupTwoCompleted( )
    {
        foreach( PlayerController player in players )
        {
            if( !player.setupTwoCompleted )
            {
                return false;
            }
        }
        return true;
    }

    // method for setup round one
    public void SetupOne( )
    {
        DebugConsole.Log( "Pick a location to place your settlement" );

        // instantiate interactables on the board
        // the interactable that is clicked on will respond
        Settlement settlement = GetLocalPlayer( ).PieceManager.GetFreePiece<Settlement>( );
        List<Intersection> availableIntersections = BoardController.GetSetupTownLocations( settlement );

        foreach( Intersection intersection in availableIntersections )
        {
            SetupOneTownInteractable interactable = Instantiate( setupOneTownInteractablePrefab
                                                               , intersection.transform.position
                                                               , Quaternion.identity );
            interactable.PieceToPlace = settlement;
            interactable.Location = intersection;
            currentInteractables.Add( interactable );
        }
    }

    // method for setup round two
    public void SetupTwo( )
    {
        DebugConsole.Log( "Pick a location to place your settlement" );

        // instantiate interactables on the board
        // the interactable that is clicked on will respond
        City city = GetLocalPlayer( ).PieceManager.GetFreePiece<City>( );
        List<Intersection> availableIntersections = BoardController.GetSetupTownLocations( city );

        foreach( Intersection intersection in availableIntersections )
        {
            SetupTwoTownInteractable interactable = Instantiate( setupTwoTownInteractablePrefab
                                                               , intersection.transform.position
                                                               , Quaternion.identity );
            interactable.PieceToPlace = city;
            interactable.Location = intersection;
            currentInteractables.Add( interactable );
        }
    }
    
    // Creates the interactables for building the passed piece during the main game phase
    public void CreateBuildInteractables( Piece piece )
    {
        //TODO (eric): Use the local player, or the piece owner?
        PlayerController player = GetLocalPlayer( );

        if( piece != null )
        {
            if( player.HasEnoughStealables( piece.Cost ) )
            {
                List<Location> availableLocations = BoardController.GetValidLocations( piece );

                if( availableLocations.Count != 0 )
                {
                    //The player is allowed to purchase the requested piece.
                    player.RemoveCards( piece.Cost );
                    SetResourceText( );

                    foreach( Location location in availableLocations )
                    {
                        Interactable interactable = Instantiate( piece.MainInteractable
                                                               , location.transform.position
                                                               , Quaternion.identity ).GetComponent<Interactable>( );
                        interactable.Location = location;
                        interactable.PieceToPlace = piece;
                        currentInteractables.Add( interactable );
                    }
                    if (piece is Settlement || piece is City)
                    {
                        player.AddVp();
                    }
                }
                else
                {
                    DebugConsole.Log( "There are no available locations to build a " + piece.GetType( ).Name );
                    (CurrentPhase as ActiveMainPhase).Action = false;
                }
            }
            else
            {
                DebugConsole.Log( "You don't have enough resources to build a " + piece.GetType( ).Name );
                (CurrentPhase as ActiveMainPhase).Action = false;
            }
        }
        else
        {
            DebugConsole.Log( "Null piece reference passed to GameController::CreateBuildInteractables.", "error" );
			(CurrentPhase as ActiveMainPhase).Action = false;
        }
    }

	// Creates the interactables for selecting a piece to move given the type of piece
	public void CreateMoveSelectionInteractables<T>() where T : Piece {
		PlayerController player = GetLocalPlayer( );

		if (player.CanMovePiece<T> ()) {
			List<T> movablePieces = player.GetMovablePieces<T> ();

			if (movablePieces.Count > 0) {

				foreach (T movablePiece in movablePieces) {
					Interactable interactable = Instantiate (movePieceSelectionInteractablePrefab
															, movablePiece.transform.position
															, Quaternion.identity).GetComponent<Interactable> ();
					interactable.PieceToMove = movablePiece;
					interactable.Location = movablePiece.Location;
					currentInteractables.Add (interactable);
				}
			} 
			else {
				DebugConsole.Log ("There is no available unit to move");
				(CurrentPhase as ActiveMainPhase).Action = false;
			}
		}
		else {
			DebugConsole.Log ("You have run out of moves for this type of unit");
			(CurrentPhase as ActiveMainPhase).Action = false;
		}
	}

	// Creates the interactables for selecting to move pirate or robber when player rolls 7
	public void CreateRobberSelectionInteractables() {
		Robber robber = pieceManager.GetPieceOnBoard<Robber> ();
		Interactable robberInteractable = Instantiate (movePieceSelectionInteractablePrefab
													  , robber.transform.position
													  , Quaternion.identity).GetComponent<Interactable> ();
		robberInteractable.PieceToMove = robber;
		robberInteractable.Location = robber.Location;
		currentInteractables.Add (robberInteractable);

		Pirate pirate = pieceManager.GetPieceOnBoard<Pirate> ();
		Interactable pirateInteractable = Instantiate (movePieceSelectionInteractablePrefab
													  , pirate.transform.position
													  , Quaternion.identity).GetComponent<Interactable> ();
		pirateInteractable.PieceToMove = pirate;
		pirateInteractable.Location = pirate.Location;
		currentInteractables.Add (pirateInteractable);
	}

	// Creates the interactables for selecting the location to move a piece to 
	public void CreateMoveToInteractables(Piece piece) {

		if (piece != null) {

			List<Location> availableLocations = BoardController.GetValidMoveLocations( piece );

			if (availableLocations.Count > 0) {

				foreach (Location location in availableLocations) {
					Interactable interactable = Instantiate (piece.MoveToInteractable
															, location.transform.position
															, Quaternion.identity).GetComponent<Interactable> ();
					interactable.PieceToMove = piece;
					interactable.Location = location;
					currentInteractables.Add (interactable);
				}
			} 
			else {
				DebugConsole.Log ("There is no available locaitons to move to");
				(CurrentPhase as ActiveMainPhase).Action = false;
			}
		} 
		else {
			DebugConsole.Log( "Null piece reference passed to GameController::CreateMoveToInteractables.", "error" );
			(CurrentPhase as ActiveMainPhase).Action = false;
		}
	}

	public void CreateDiscardCityInteractables() {
		PlayerController player = GetLocalPlayer ();

		foreach (City city in player.PieceManager.GetPiecesOnBoard<City>()) {
			Interactable interactable = Instantiate (city.DiscardInteractable
													, city.Location.transform.position
													, Quaternion.identity).GetComponent<Interactable> ();
			interactable.Location = city.Location;
			currentInteractables.Add (interactable);
		}
	}

	public void CreateActivateKnightInteractables() {
		PlayerController player = GetLocalPlayer ();

		if (player.HasEnoughStealables (new Dictionary<Card, int> { { new ResourceCard (ResourceType.Grain), 1 } })) {

			foreach (Knight knight in player.PieceManager.GetPiecesOnBoard<Knight>()) {
				if (!knight.active) {
					Interactable interactable = Instantiate (knight.ActivateInteractable
														, knight.Location.transform.position
														, Quaternion.identity).GetComponent<Interactable> ();
					interactable.Location = knight.Location;
					currentInteractables.Add (interactable);
				}
			}
		} 
		else {
			DebugConsole.Log ("You don't have enough resources to activate a knight");
		}
	}

    public void AddInteractable( Interactable interactable )
    {
        currentInteractables.Add( interactable );
    }

    public void DestroyInteractables( )
    {
        //Iterate in reverse over the interactable list, destroying them and
        //then removing the destroyed reference from the list.
        for( int i = currentInteractables.Count - 1; i >= 0; i-- )
        {
            Destroy( currentInteractables[i].gameObject );
            currentInteractables.RemoveAt( i );
        }
    }

    // method for determining which player should go first
    public int[] DeterminePlayerOrder( )
    {
        //TODO: Reorder the server's players list here

        //Order the players in simple array form
        int[] playerOrder = new int[players.Count];

        for( int i = 0; i < players.Count; i++ )
        {
            playerOrder[i] = (int)players[i].netId.Value;
        }

        //Inform all clients of the correct list order
        return playerOrder;
    }

    public void AddConnectedPlayer( PlayerController player )
    {
        players.Add( player );
    }

    public void RemoveConnectedPlayer( int netId )
    {
        for( int i = 0; i < players.Count; i++ )
        {
            if( players[i].netId.Value == netId )
            {
                players.RemoveAt( i );
            }
        }
    }

    //initialize a set of cards
    public void CreateCards( )
    {
        ResourceCards.Add( new ResourceCard( ResourceType.Brick ), 19 );
        ResourceCards.Add( new ResourceCard( ResourceType.Grain ), 19 );
        ResourceCards.Add( new ResourceCard( ResourceType.Lumber ), 19 );
        ResourceCards.Add( new ResourceCard( ResourceType.Ore ), 19 );
        ResourceCards.Add( new ResourceCard( ResourceType.Wool ), 19 );

        CommodityCards.Add( new CommodityCard( CommodityType.Coin ), 12 );
        CommodityCards.Add( new CommodityCard( CommodityType.Paper ), 12 );
        CommodityCards.Add( new CommodityCard( CommodityType.Cloth ), 12 );

        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.CommercialHarbor ), 2 );
        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.MasterMerchant ), 2 );
        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.Merchant ), 6 );
        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.MerchantFleet ), 2 );
        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.ResouceMonopoly ), 4 );
        TradeCards.Add( new ProgressCard( ProgressKind.Trade, ProgressType.TradeMonopoly ), 2 );

        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Bishop ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Constitution ), 1 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Deserter ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Diplomat ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Intrigue ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Saboteur ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Spy ), 3 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Warlord ), 2 );
        PoliticsCards.Add( new ProgressCard( ProgressKind.Politics, ProgressType.Wedding ), 2 );

        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Alchemist ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Crane ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Engineer ), 1 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Inventor ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Irrigation ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Medicine ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Mining ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Printer ), 1 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.RoadBuilding ), 2 );
        ScienceCards.Add( new ProgressCard( ProgressKind.Science, ProgressType.Smith ), 2 );
    }

    //change the number of cards in bank
    public void AddCards( Dictionary<Card, int> cardsToReturn )
    {
        foreach( KeyValuePair<Card, int> entry in cardsToReturn )
        {
            if( entry.Key is ResourceCard )
            {
                ResourceCards[entry.Key as ResourceCard] += entry.Value;
            }
            else if( entry.Key is CommodityCard )
            {
                CommodityCards[entry.Key as CommodityCard] += entry.Value;
            }
            else if( entry.Key is ProgressCard )
            {
                if( TradeCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    TradeCards[entry.Key as ProgressCard] += entry.Value;
                }
                else if( PoliticsCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    PoliticsCards[entry.Key as ProgressCard] += entry.Value;
                }
                else if( ScienceCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    ScienceCards[entry.Key as ProgressCard] += entry.Value;
                }
            }
        }
    }

    //change the number of cards in deck
    public void AddCards( String[] cards )
    {
        foreach( String cardName in cards )
        {
            //We cannot enumerate over a dictionary and modify its values at
            //the same time. To work around this, we create a list of the
            //dictionary's Keys, enumerate over that, and modify the value when
            //we find a match.
            List<ResourceCard> resourceCardTypes = new List<ResourceCard>( ResourceCards.Keys );

            foreach( ResourceCard resourceCardType in resourceCardTypes )
            {
                if( resourceCardType.type.ToString( ) == cardName )
                {
                    //Increment the resource type in the dictionary.
                    ResourceCards[resourceCardType]++;
                    continue;
                }
            }

            List<CommodityCard> commodityCardTypes = new List<CommodityCard>( CommodityCards.Keys );

            foreach( CommodityCard commodityCardType in commodityCardTypes )
            {
                if( commodityCardType.type.ToString( ) == cardName )
                {
                    //Increment the resource type in the dictionary.
                    CommodityCards[commodityCardType]++;
                    continue;
                }
            }

            List<ProgressCard> tradeCardTypes = new List<ProgressCard>( TradeCards.Keys );

            foreach( ProgressCard tradeCardType in tradeCardTypes )
            {
                if( tradeCardType.type.ToString( ) == cardName )
                {
                    TradeCards[tradeCardType]++;
                    continue;
                }
            }

            List<ProgressCard> politicsCardTypes = new List<ProgressCard>( PoliticsCards.Keys );

            foreach( ProgressCard politicsCardType in politicsCardTypes )
            {
                if( politicsCardType.type.ToString( ) == cardName )
                {
                    TradeCards[politicsCardType]++;
                    continue;
                }
            }

            List<ProgressCard> scienceCardTypes = new List<ProgressCard>( ScienceCards.Keys );

            foreach( ProgressCard scienceCardType in scienceCardTypes )
            {
                if( scienceCardType.type.ToString( ) == cardName )
                {
                    TradeCards[scienceCardType]++;
                    continue;
                }
            }

        }

        SetResourceText( );
    }

    //change the number of cards in bank
    public void RemoveCards( Dictionary<Card, int> cardsToDraw )
    {
        foreach( KeyValuePair<Card, int> entry in cardsToDraw )
        {
            if( entry.Key is ResourceCard )
            {
                ResourceCards[entry.Key as ResourceCard] -= entry.Value;
            }
            else if( entry.Key is CommodityCard )
            {
                CommodityCards[entry.Key as CommodityCard] -= entry.Value;
            }
            else if( entry.Key is ProgressCard )
            {
                if( TradeCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    TradeCards[entry.Key as ProgressCard] -= entry.Value;
                }
                else if( PoliticsCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    PoliticsCards[entry.Key as ProgressCard] -= entry.Value;
                }
                else if( ScienceCards.ContainsKey( entry.Key as ProgressCard ) )
                {
                    ScienceCards[entry.Key as ProgressCard] -= entry.Value;
                }
            }
        }
    }

    //change the number of cards in deck
    public void RemoveCards( String[] cards )
    {
        foreach( String cardName in cards )
        {
            //We cannot enumerate over a dictionary and modify its values at
            //the same time. To work around this, we create a list of the
            //dictionary's Keys, enumerate over that, and modify the value when
            //we find a match.
            List<ResourceCard> resourceCardTypes = new List<ResourceCard>( ResourceCards.Keys );

            foreach( ResourceCard resourceCardType in resourceCardTypes )
            {
                if( resourceCardType.type.ToString( ) == cardName )
                {
                    //Increment the resource type in the dictionary.
                    ResourceCards[resourceCardType]--;
                    continue;
                }
            }

            List<CommodityCard> commodityCardTypes = new List<CommodityCard>( CommodityCards.Keys );

            foreach( CommodityCard commodityCardType in commodityCardTypes )
            {
                if( commodityCardType.type.ToString( ) == cardName )
                {
                    //Increment the resource type in the dictionary.
                    CommodityCards[commodityCardType]--;
                    continue;
                }
            }

            List<ProgressCard> tradeCardTypes = new List<ProgressCard>( TradeCards.Keys );

            foreach( ProgressCard tradeCardType in tradeCardTypes )
            {
                if( tradeCardType.type.ToString( ) == cardName )
                {
                    TradeCards[tradeCardType]--;
                    continue;
                }
            }

            List<ProgressCard> politicsCardTypes = new List<ProgressCard>( PoliticsCards.Keys );

            foreach( ProgressCard politicsCardType in politicsCardTypes )
            {
                if( politicsCardType.type.ToString( ) == cardName )
                {
                    TradeCards[politicsCardType]--;
                    continue;
                }
            }

            List<ProgressCard> scienceCardTypes = new List<ProgressCard>( ScienceCards.Keys );

            foreach( ProgressCard scienceCardType in scienceCardTypes )
            {
                if( scienceCardType.type.ToString( ) == cardName )
                {
                    TradeCards[scienceCardType]--;
                    continue;
                }
            }

        }

        SetResourceText( );
    }

    public List<ProgressCard> AvailableTradeCards( )
    {
        List<ProgressCard> availableTypes = new List<ProgressCard>( );

        foreach( KeyValuePair<ProgressCard, int> entry in TradeCards )
        {
            if( entry.Value > 0 )
            {
                availableTypes.Add( entry.Key );
            }
        }

        return availableTypes;
    }

    public ProgressCard GetRandomAvailableTradeCard( )
    {
        List<ProgressCard> availableTypes = AvailableTradeCards( );

        System.Random rand = new System.Random( );

        return availableTypes[rand.Next( availableTypes.Count )];
    }

    public List<ProgressCard> AvailablePoliticsCards( )
    {
        List<ProgressCard> availableTypes = new List<ProgressCard>( );

        foreach( KeyValuePair<ProgressCard, int> entry in PoliticsCards )
        {
            if( entry.Value > 0 )
            {
                availableTypes.Add( entry.Key );
            }
        }

        return availableTypes;
    }

    public ProgressCard GetRandomAvailablePoliticsCard( )
    {
        List<ProgressCard> availableTypes = AvailablePoliticsCards( );

        System.Random rand = new System.Random( );

        return availableTypes[rand.Next( availableTypes.Count )];
    }

    public List<ProgressCard> AvailableScienceCards( )
    {
        List<ProgressCard> availableTypes = new List<ProgressCard>( );

        foreach( KeyValuePair<ProgressCard, int> entry in ScienceCards )
        {
            if( entry.Value > 0 )
            {
                availableTypes.Add( entry.Key );
            }
        }

        return availableTypes;
    }

    public ProgressCard GetRandomAvailableScienceCard( )
    {
        List<ProgressCard> availableTypes = AvailableScienceCards( );

        System.Random rand = new System.Random( );

        return availableTypes[rand.Next( availableTypes.Count )];
    }

    public void UpdateDiceText( )
    {
        StringBuilder diceTextBuilder = new StringBuilder( );
        diceTextBuilder.Append( _yellowDie.ToString( ) + "\n" );
        diceTextBuilder.Append( _redDie.ToString( ) + "\n" );
        diceTextBuilder.Append( _eventDie.ToString( ) + "\n" );
        diceText.text = diceTextBuilder.ToString( );
    }

    //TODO (eric): Consider implementing "RollEvent" classes, which do something based on the
    //             values of "Die" objects attached to them.
    public void RollDice( )
    {
		StartCoroutine (HandleRollDice ());
    }

	private IEnumerator HandleRollDice() {
		//Roll the die
		_redDie.Roll( );
		_yellowDie.Roll( );
		_eventDie.Roll( );

		if( _eventDie.GetLastRollAsEvent( ) == EventDie.EventType.Barbarian )
		{
			_barbarianPosition--;

			if( _barbarianPosition <= 0 )
			{
				int barbarianStrength = 0;
				int knightStrength = 0;
				Dictionary<PlayerController, int> playerStrengths = new Dictionary<PlayerController, int>( );

				foreach( PlayerController player in players )
				{
					//Calculate barbarian strength
					List<City> playerCities = player.PieceManager.GetPiecesOnBoard<City>( );
					barbarianStrength += playerCities.Count;

					//Calculate player knight strength
					playerStrengths.Add( player, 0 ); 
					List<Knight> playerKnights = player.PieceManager.GetPiecesOnBoard<Knight>( );

					foreach( Knight knight in playerKnights )
					{
						playerStrengths[player] += knight.Strength;
						knightStrength += knight.Strength;
					}
				}

				if( barbarianStrength > knightStrength )
				{
					//Barbarians Win
					List<PlayerController> lowestContributors = new List<PlayerController>( );
					int lowestContribution = Int32.MaxValue;

					foreach( KeyValuePair<PlayerController, int> playerToStrength in playerStrengths )
					{
						if( playerToStrength.Value < lowestContribution )
						{
							lowestContributors.Clear( );
							lowestContributors.Add( playerToStrength.Key );
							lowestContribution = playerToStrength.Value;
						}
						else if( playerToStrength.Value == lowestContribution )
						{
							lowestContributors.Add( playerToStrength.Key );
						}
					}

					foreach( PlayerController player in lowestContributors )
					{
						List<City> playerCities = player.PieceManager.GetPiecesOnBoard<City>( );
						if( playerCities.Count == 1 )
						{
							//The player has one city, automatically remove it.
							Location cityLocation = playerCities[0].Location;
							RpcRemovePieceFromBoard( playerCities[0].gameObject );
							RpcPlacePieceOnBoard( player.PieceManager.GetFreePiece<Settlement>( ).gameObject
								, cityLocation.gameObject );
						}
						else
						{
							// send messages to player to discard a city
							player.RpcRespondToDiscardCity ();
						}
					}

					// wait for all the players who need to discard cities to reply
					while (GetLocalPlayer ().receivedReply < lowestContributors.Count) {
						yield return new WaitForSeconds (0.1f);
					}

					GetLocalPlayer ().receivedReply = 0;
				}
				else
				{
					//Barbarians Lose
					List<PlayerController> highestContributors = new List<PlayerController>( );
					int highestContribution = Int32.MinValue;

					foreach( KeyValuePair<PlayerController, int> playerToStrength in playerStrengths )
					{
						if( playerToStrength.Value > highestContribution )
						{
							highestContributors.Clear( );
							highestContributors.Add( playerToStrength.Key );
							highestContribution = playerToStrength.Value;
						}
						else if( playerToStrength.Value == highestContribution )
						{
							highestContributors.Add( playerToStrength.Key );
						}
					}

					if( highestContributors.Count == 1 )
					{
						//Award Defender of Catan VP.
					}
					else
					{
						foreach (PlayerController player in highestContributors) {
							player.RpcRespondToDrawProgressCard ();
						}
							
						while (GetLocalPlayer ().receivedReply < highestContributors.Count) {
							yield return new WaitForSeconds (0.1f);
						}

						GetLocalPlayer ().receivedReply = 0;
					}
				}

				//Reset barbarian position
				_barbarianPosition = BARBARIAN_STARTING_DISTANCE;

				Robber robber = pieceManager.GetPieceOnBoard<Robber> ();
				Pirate pirate = pieceManager.GetPieceOnBoard<Pirate> ();

				if (!robber.isActivated() && !pirate.isActivated()) {
					robber.Activate ();
					pirate.Activate ();

					Location robberLocation = BoardController.GetRobberLocation ();
					if (robberLocation != null) {
						RpcPlacePieceOnBoard (robber.gameObject, robberLocation.gameObject);
					} 
					else {
						DebugConsole.Log ("Could not find Robber starting position.");
					}

					Location pirateLocation = BoardController.GetPirateLocation ();
					if (pirateLocation != null) {
						RpcPlacePieceOnBoard (pirate.gameObject, pirateLocation.gameObject);
					} 
					else {
						DebugConsole.Log ("Could not find Pirate starting position.");
					}
				}
			}
		}
		else
		{
			//Do Progress Card Interaction
			//TODO (eric): Think about how cards are handled
		}

		int rollSum = _redDie.LastRoll + _yellowDie.LastRoll;

		//Do Hex Production
		if (rollSum == 7) {
			//yield return HandleRobberEvent ();

			//TODO (eric): Think about how cards are handled.
		} 
		else {
			List<Hex> producingHexes = BoardController.GetHexesWithNumber (rollSum);

			foreach (Hex hex in producingHexes) {
				foreach (KeyValuePair<int, Intersection> kvPair in hex.Intersections) {
					Intersection intersection = kvPair.Value;
					if (intersection.HasPiece<Town> ()) {
						//get the town
						Town town = intersection.GetPiece<Town> ();
						PlayerController p = town.Owner;

						TerrainKind terrain = hex.terrainKind;
						ResourceType cardtype = town.converter (terrain);

						//TODO (eric): Need to take into account gold fields.
						if (town is Settlement) {
							p.RpcAddCards( new String[] { cardtype.ToString( ) } );
						} 
						else {
							//TODO (eric): When we add commodity cards, add a commodity instead.
							p.RpcAddCards( new String[] { cardtype.ToString( ), cardtype.ToString( ) } );
						}
					}
				}
			}
		}

		SetResourceText( );

		// activate required UI elements for after rolling dice
		players[activePlayerIndex].RpcSetUI();
	}

	private IEnumerator HandleRobberEvent() {
		int numPlayersWithExtraCards = 0;
		// loop through each player that is not the local player
		foreach (PlayerController player in players) {
			// count the number of resource cards that the player has
			// if it is greater than 7, send message to the player to discard cards
			if (player.GetNumStealables() > 7) {
				player.RpcRespondToDiscardCards (player.GetNumStealables() / 2);
				numPlayersWithExtraCards++;
			}
		}

		while (GetLocalPlayer ().receivedReply < numPlayersWithExtraCards) {
			yield return new WaitForSeconds (0.1f);
		}

		GetLocalPlayer ().receivedReply = 0;

		Robber robber = pieceManager.GetPieceOnBoard<Robber>();
		Pirate pirate = pieceManager.GetPieceOnBoard<Pirate> ();

		// only allow the active player to move the robber or pirate and steal from other players after the first barbarian attack 
		if (robber.isActivated() && pirate.isActivated()) {
			players [activePlayerIndex].RpcRespondToMoveRobber ();

			while (GetLocalPlayer ().receivedReply < 1) {
				yield return new WaitForSeconds (0.1f);
			}

			GetLocalPlayer ().receivedReply = 0;
		}

	}

	public IEnumerator RespondToMoveRobber() {
		// remember the location of the robber and pirate, use it later to find out which one the player moved
		Robber robber = pieceManager.GetPieceOnBoard<Robber>();
		Location originalRobberLocation = robber.Location;
		Pirate pirate = pieceManager.GetPieceOnBoard<Pirate> ();
		Location originalPirateLocation = pirate.Location;

		// allow the active player to move the robber or pirate
		(CurrentPhase as ActiveMainPhase).Action = true;
		CreateRobberSelectionInteractables ();

		while ((CurrentPhase as ActiveMainPhase).Action) {
			yield return new WaitForSeconds (0.1f);
		}

		List<PlayerController> availablePlayers = new List<PlayerController>();
		Hex hexLocation;

		if (robber.Location != originalRobberLocation) {
			hexLocation = robber.Location as Hex;
			foreach (PlayerController p in players) {
				if (!p.isLocalPlayer && hexLocation.HasPlayerTown (p)) {
					availablePlayers.Add (p);
				}
			}
		} 
		else if (pirate.Location != originalPirateLocation) {
			hexLocation = pirate.Location as Hex;
			foreach (PlayerController p in players) {
				if (!p.isLocalPlayer && hexLocation.HasPlayerShip (p)) {
					availablePlayers.Add (p);
				}
			}
		}

		// TODO: method to ask local player to choose available player to steal from through UI


		// send a reply back to server, which is waiting in rolldice
		CmdReceiveReply ();
	}

	[Command]
	public void CmdReceiveReply() {
		GetLocalPlayer ().receivedReply++;
	}
    
    [ClientRpc]
    public void RpcRoundTwoAssignResources( GameObject pieceObject )
    {
        Town town = pieceObject.GetComponent<Town>( );

        List<Hex> hexes = (town.Location as Intersection).Hexes;

        foreach( Hex hex in hexes )
        {
            //TODO (eric): Can we guarantee hex is not null here?
            if( hex != null )
            {
                TerrainKind terrain = hex.terrainKind;
                ResourceType cardtype = town.converter( terrain );

                //TODO (eric): Need to take into account gold fields.
                if( town is Settlement )
                {
                    town.Owner.AddCards( new Dictionary<Card,int> { { new ResourceCard( cardtype ), 1 } } );
                }
                else
                {
                    //TODO (eric): When we add commodity cards, add a commodity instead.
                    town.Owner.AddCards( new Dictionary<Card, int> { { new ResourceCard( cardtype ), 2 } } );
                }
            }
        }

        SetResourceText( );
    }

    public void SetResourceText( )
    {
        PlayerController localPlayer = GetLocalPlayer( );
        List<Card> keys = new List<Card>( localPlayer.StealableCards.Keys );
        string text = "";
        foreach( Card key in keys )
        {
            if( key is ResourceCard )
            {
                text = text + (key as ResourceCard).type.ToString( ) + ": " + localPlayer.StealableCards[key].ToString( ) + " ";
            }
        }
        resourceText.text = text;
    }

    void SetColourText( )
    {
        colourText.text = "Your colour: " + GetLocalPlayer( ).Color.ToString( );
        colourText.color = GetLocalPlayer( ).Color;
    }

    public void startTrade( int a, int b, PlayerController p )
    {
        Dictionary<Card, Card> offer = new Dictionary<Card, Card>( );
        Card aCard;
        Card bCard;
        switch( a )
        {
            case 0:
                aCard = new ResourceCard( ResourceType.Brick );
                break;
            case 1:
                aCard = new ResourceCard( ResourceType.Grain );
                break;
            case 2:
                aCard = new ResourceCard( ResourceType.Lumber );
                break;
            case 3:
                aCard = new ResourceCard( ResourceType.Ore );
                break;
            case 4:
                aCard = new ResourceCard( ResourceType.Wool );
                break;
            default:
                aCard = new ResourceCard( ResourceType.Brick );
                break;
        }

        switch( b )
        {
            case 0:
                bCard = new ResourceCard( ResourceType.Brick );
                break;
            case 1:
                bCard = new ResourceCard( ResourceType.Grain );
                break;
            case 2:
                bCard = new ResourceCard( ResourceType.Lumber );
                break;
            case 3:
                bCard = new ResourceCard( ResourceType.Ore );
                break;
            case 4:
                bCard = new ResourceCard( ResourceType.Wool );
                break;
            default:
                bCard = new ResourceCard( ResourceType.Brick );
                break;
        }
        if( p != players[activePlayerIndex] )
            return;

        offer.Add( aCard, bCard );
        MaritimeTrade.Trade( offer, this, players[activePlayerIndex] );
    }

    public SerializableGame AsSerializable( )
    {
        SerializableGame output = new SerializableGame( );

        output.redDie = _redDie.LastRoll;
        output.yellowDie = _yellowDie.LastRoll;
        output.eventDie = _eventDie.LastRoll;

        output.currentPhase = _currentPhase;

        output._barbarianPosition = _barbarianPosition;

        output._activePlayerIndex = activePlayerIndex;

        output._resourceCards = ResourceCards;
        output._commodityCards = CommodityCards;
        output._tradeCards = TradeCards;
        output._politicsCards = PoliticsCards;
        output._scienceCards = ScienceCards;

        //The "players" list is ordered in the turn order.
        output._players = new List<SerializablePlayer>( );
        foreach( PlayerController player in players )
        {
            output._players.Add( player.AsSerializable( ) );
        }

        output._boardHexes = new List<SerializableHex>( );
        foreach( Hex hex in BoardController.Hexes )
        {
            //The list returned from Hex.AsSerializable should contain only a single SerializableHex
            output._boardHexes.Add( hex.AsSerializable( )[0] );
        }

        return output;
    }

    //Serializes the game state to a file with the specified name.
    public void SaveGame( string filename )
    {
        BinaryFormatter formatter = new BinaryFormatter( );
        FileStream file = File.Create( Application.persistentDataPath + "/" + filename + ".dat" );

        formatter.Serialize( file, this.AsSerializable( ) );
        file.Close( );
    }

    //Attempts to load a game state from a file with the specified name.
    public void LoadGame( string filename )
    {
        if( File.Exists( Application.persistentDataPath + "/" + filename ) )
        {
            BinaryFormatter formatter = new BinaryFormatter( );
            FileStream file = File.Open( Application.persistentDataPath + "/" + filename, FileMode.Open );

            //We'll store this information, and check for its existence when we start the match.
            _loadedGame = formatter.Deserialize( file ) as SerializableGame;

            file.Close( );
        }
    }

    [ClientRpc]
    public void RpcStartWithLoadedData( Byte[] serializedData )
    {
        BinaryFormatter formatter = new BinaryFormatter( );
        MemoryStream ms = new MemoryStream( );
        ms.Write( serializedData, 0, serializedData.Length );
        ms.Seek( 0, SeekOrigin.Begin );
        _loadedGame = formatter.Deserialize( ms ) as SerializableGame;

        //Now that this client has the loaded game data, we need to initialize all of its state.
        //Ignore _boardHexes, assume we always use the same board for now.

        foreach( SerializablePlayer serializedPlayer in _loadedGame._players )
        {
            PlayerController networkPlayer = FindPlayerWithColorId( serializedPlayer.colorId );

            networkPlayer.movedShip = serializedPlayer._movedShip;
            networkPlayer.receivedReply = serializedPlayer._receivedReply;
            networkPlayer._stealableCards = serializedPlayer._stealableCards;
            networkPlayer._progressCards = serializedPlayer._progressCards;

            if( NetworkServer.active )
            {
                //If we're on the server, we want to pick pieces and tell the clients which pieces we placed, and where.

                foreach( SerializablePiece piece in serializedPlayer._settlements )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<Settlement>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._cities )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<City>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._roads )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<Road>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._ships )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<Ship>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._basicKnights )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<BasicKnight>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._strongKnights )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<StrongKnight>( ).gameObject, location.gameObject );
                    }
                }

                foreach( SerializablePiece piece in serializedPlayer._mightyKnights )
                {
                    if( piece._location != null )
                    {
                        //This piece was placed on the board. Get its location
                        Location location = BoardController.GetLocationFromSerializable( piece._location );
                        RpcPlacePieceOnBoard( networkPlayer.PieceManager.GetFreePiece<MightyKnight>( ).gameObject, location.gameObject );
                    }
                }
            }
        }

        //Set the cards in the bank
        ResourceCards = _loadedGame._resourceCards;
        CommodityCards = _loadedGame._commodityCards;
        TradeCards = _loadedGame._tradeCards;
        PoliticsCards = _loadedGame._politicsCards;
        ScienceCards = _loadedGame._scienceCards;

        //Set the die
        _redDie.LastRoll = _loadedGame.redDie;
        _yellowDie.LastRoll = _loadedGame.yellowDie;
        _eventDie.LastRoll = _loadedGame.eventDie;

        _barbarianPosition = _loadedGame._barbarianPosition;
        activePlayerIndex = _loadedGame._activePlayerIndex;

        //Ignore the _loadedGame phase because we can only save during the MainPhase
        if( IsActivePlayer( GetLocalPlayer( ) ) )
        {
            CurrentPhase = new ActiveMainPhase( );
        }
        else
        {
            CurrentPhase = new PassiveMainPhase( );
        }
    }

    public PlayerController FindPlayerWithColorId( int colorId )
    {
        PlayerController output = null;

        foreach( PlayerController player in players )
        {
            if( player._colorId == colorId )
            {
                output = player;
                break;
            }
        }

        return output;
    }
}

[Serializable]
public class SerializableGame
{
    //Board.
    public List<SerializableHex> _boardHexes;

    //Players. Includes the players owned pieces.
    public List<SerializablePlayer> _players;

    //Current player. The order of _players is the same as the player order.
    public int _activePlayerIndex;

    public int _barbarianPosition;

    //Cards
    public Dictionary<ResourceCard, int> _resourceCards;
    public Dictionary<CommodityCard, int> _commodityCards;
    public Dictionary<ProgressCard, int> _tradeCards;
    public Dictionary<ProgressCard, int> _politicsCards;
    public Dictionary<ProgressCard, int> _scienceCards;

    //Last roll
    public int redDie;
    public int yellowDie;
    public int eventDie;

    public Phase currentPhase;
    
    //TODO: Remember GameController Pieces
    //TODO: Remember GameController cards (remaining in bank)
}