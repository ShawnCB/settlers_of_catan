﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;

public class MatchList : MonoBehaviour
{
    public CatanNetworkManager networkManager;
    public SimpleObjectPool matchButtonPool;
    public Transform contentPanel;

    public void Update( )
    {
        //This involves network calls and probably shouldn't be called every frame.
        //But it will work if it is here.
    }

    public void RefreshList( )
    {
        networkManager.matchMaker.ListMatches( 0, 4, "", false, 0, 0, networkManager.OnMatchList );
    }

    public void RecreateButtons( List<MatchInfoSnapshot> matchInfoList )
    {
        RemoveButtons( );
        AddButtons( matchInfoList );
    }

    private void RemoveButtons( )
    {
        while( contentPanel.transform.childCount > 0 )
        {
            GameObject toRemove = transform.GetChild( 0 ).gameObject;
            matchButtonPool.ReturnObject( toRemove );
        }
    }

    private void AddButtons( List<MatchInfoSnapshot> matchInfoList )
    {
        foreach( MatchInfoSnapshot matchInfo in matchInfoList )
        {
            GameObject newMatchButton = matchButtonPool.GetObject( );
            newMatchButton.transform.SetParent( contentPanel.transform );
            newMatchButton.GetComponent<MatchButton>( ).Setup( matchInfo );
        }
    }
}