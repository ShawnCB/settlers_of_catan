﻿using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPhaseUI : MonoBehaviour
{
    public Button _startMatchButton;
    public Button _loadGameButton;
    public ToggleGroup _toggleGroup;
    public FileList _fileList;
    public CatanNetworkManager _networkManager;
    private GameController _gameController;

    // Use this for initialization
    void Start( )
    {
        _networkManager.StartMatchMaker( );
    }

    public void StartMatch( )
    {
        if( _gameController == null )
        {
            _gameController = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );
            if( _gameController == null )
            {
                DebugConsole.Log( "Could not find GameController object", "error" );
                return;
            }
        }

        if( !_toggleGroup.AnyTogglesOn( ) )
        {
            DebugConsole.Log( "You must select a color.", "warning" );
            return;
        }

        //There should only be one active toggle in ActiveToggles( )
        int colorId = -1;
        foreach( Toggle activeToggle in _toggleGroup.ActiveToggles( ) )
        {
            colorId = activeToggle.gameObject.GetComponent<ColorToggle>( ).colorId;
        }

        List<PlayerController> connectedPlayers = GameController.GetConnectedPlayers( );

        if( _gameController.AllPlayersSelectedColor( ) )
        {
            //Force a synchronization of colors.
            foreach( PlayerController player in _gameController.players )
            {
                player.RpcSetPlayerColor( player._colorId );
            }

            //Colors have been selected and are all different
            //Start game
            // Determine the order of play, informing all clients of it.
            int[] playerOrder = _gameController.DeterminePlayerOrder( );
            _gameController.RpcSetPlayerOrder( playerOrder );

            // Spawn the networked game objects that will be used during this game.
            _gameController.SpawnGameObjects( );

            if( _gameController._loadedGame != null )
            {
                //We have loaded data that we need to initialize
                MemoryStream ms = new MemoryStream( );
                BinaryFormatter bf = new BinaryFormatter( );
                bf.Serialize( ms, _gameController._loadedGame );
                _gameController.RpcStartWithLoadedData( ms.ToArray( ) );
            }
            else
            {
                // Signal to all clients to start the setup phase.
                _gameController.RpcStartSetupOnePhase( );
            }
        }
        else
        {
            DebugConsole.Log( "Not all players have selected a color.", "warning" );
            DebugConsole.Log( "Please wait until all have selected a non-conflicting color", "warning" );
        }
    }

    public void LoadGame( )
    {
        if( _fileList.gameObject.activeInHierarchy )
        {
            //Since the list is active, hide it.
            _fileList.gameObject.SetActive( false );
        }
        else
        {
            //Since the list is inactive, show it.
            _fileList.RefreshList( );
            _fileList.gameObject.SetActive( true );
        }
    }
}
