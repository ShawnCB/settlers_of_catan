﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPhaseUI : MonoBehaviour
{
    public LocalPlayerButton _localPlayerButton;
    public LocalPlayerPanel _localPlayerPanel;
    public VerticalLayoutGroup _otherPlayerPanels;
    public OtherPlayerPanel _otherPlayerPanel1;
    public OtherPlayerPanel _otherPlayerPanel2;
    public OtherPlayerPanel _otherPlayerPanel3;
    public VerticalLayoutGroup _otherPlayerButtons;
    public Button _otherPlayerButton1;
    public Button _otherPlayerButton2;
    public Button _otherPlayerButton3;
    public Button _endTurnButton;
    public Button _rollDiceButton;
    public Button _mainMenuButton;
    public GameObject _mainMenuPanel;
    public Button _actionButton;
    public VerticalLayoutGroup _actionPanel;
    public InputField _saveFileInput;
    public GameObject _bankTradeWindow;
    public ProgressCardList _progressCardList;
	public GameObject _chooseProgressCardPanel;

    private Dictionary<PlayerController, int> playerControllerToPanelNumber = new Dictionary<PlayerController, int>( );

    // reference to the game
    private GameController _gameController = null;
    private GameController GameController
    {
        get
        {
            //Note: We used to find the GameController object in
            //      PlayerController::Start. However, clients can create
            //      PlayerController objects before the GameController object
            //      has been created, causing errors. Therefore, we simply
            //      delay retrieval of the GameController object until it is
            //      first required.
            //      
            //      This can potentially return null if called before the
            //      GameController has been constructed, so take care in when
            //      you ask for it.
            if( _gameController == null )
            {
                //Expects us to only ever have at most one GameController in the scene.
                GameObject gameControllerObj = GameObject.FindWithTag( "GameController" );
                if( gameControllerObj != null )
                {
                    _gameController = gameControllerObj.GetComponent<GameController>( );
                }
            }
            return _gameController;
        }
        set
        {
            _gameController = value;
        }
    }

    public void RegisterPlayer( PlayerController player, int panelNum )
    {
        playerControllerToPanelNumber.Add( player, panelNum );

        switch( panelNum )
        {
            case 1:
                _otherPlayerButton1.gameObject.SetActive( true );
                _otherPlayerPanel1.gameObject.SetActive( true );
                break;
            case 2:
                _otherPlayerButton2.gameObject.SetActive( true );
                _otherPlayerPanel2.gameObject.SetActive( true );
                break;
            case 3:
                _otherPlayerButton3.gameObject.SetActive( true );
                _otherPlayerPanel3.gameObject.SetActive( true );
                break;
        }
    }

    public void UpdatePlayer( PlayerController player )
    {
        int panelNum = playerControllerToPanelNumber[player];

        Dictionary<Card,int> stealables = player.StealableCards;
        Dictionary<ProgressCard, int> progressCards = player.ProgressCards;

        int progressCardCount = 0;
        foreach( KeyValuePair<ProgressCard, int> entry in progressCards )
        {
            progressCardCount += entry.Value;
        }

        if( panelNum == 0 )
        {
            //Should be local player
            _progressCardList.RefreshList( );

            //Refresh Pieces
            _localPlayerPanel._settlementsCountText.text = player.PieceManager.GetPiecesOnBoard<Settlement>( ).Count.ToString( );
            _localPlayerPanel._citiesCountText.text = player.PieceManager.GetPiecesOnBoard<City>( ).Count.ToString( );
            //_localPlayerPanel._metropolisCountText.text = player.PieceManager.GetPiecesOnBoard<Metropolis>( ).Count.ToString( );
            _localPlayerPanel._basicKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<BasicKnight>( ).Count.ToString( );
            _localPlayerPanel._strongKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<StrongKnight>( ).Count.ToString( );
            _localPlayerPanel._mightyKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<MightyKnight>( ).Count.ToString( );
            _localPlayerPanel._roadsCountText.text = player.PieceManager.GetPiecesOnBoard<Road>( ).Count.ToString( );
            _localPlayerPanel._shipsCountText.text = player.PieceManager.GetPiecesOnBoard<Ship>( ).Count.ToString( );

            //Refresh Cards
            foreach( KeyValuePair<Card, int> entry in stealables )
            {
                if( entry.Key is ResourceCard )
                {
                    switch( (entry.Key as ResourceCard).type )
                    {
                        case ResourceType.Ore:
                            _localPlayerButton._oreAmountText.text = entry.Value.ToString( );
                            break;
                        case ResourceType.Wool:
                            _localPlayerButton._woolAmountText.text = entry.Value.ToString( );
                            break;
                        case ResourceType.Brick:
                            _localPlayerButton._brickAmountText.text = entry.Value.ToString( );
                            break;
                        case ResourceType.Grain:
                            _localPlayerButton._grainAmountText.text = entry.Value.ToString( );
                            break;
                        case ResourceType.Lumber:
                            _localPlayerButton._lumberAmountText.text = entry.Value.ToString( );
                            break;
                    }
                }
                else if( entry.Key is CommodityCard )
                {
                    switch( (entry.Key as CommodityCard).type )
                    {
                        case CommodityType.Cloth:
                            _localPlayerButton._oreAmountText.text = entry.Value.ToString( );
                            break;
                        case CommodityType.Coin:
                            _localPlayerButton._woolAmountText.text = entry.Value.ToString( );
                            break;
                        case CommodityType.Paper:
                            _localPlayerButton._brickAmountText.text = entry.Value.ToString( );
                            break;
                    }
                }
            }

            _localPlayerButton._progressCardAmountText.text = progressCardCount.ToString( );
            _progressCardList.RefreshList( );

            //TODO (eric): Get or Calculate Victory Points;
        }
        else
        {
            int handSize = 0;

            foreach( KeyValuePair<Card, int> entry in stealables )
            {
                handSize += entry.Value;
            }

            OtherPlayerPanel panel;

            switch( panelNum )
            {
                case 1:
                    panel = _otherPlayerPanel1;
                    break;
                case 2:
                    panel = _otherPlayerPanel2;
                    break;
                case 3:
                    panel = _otherPlayerPanel3;
                    break;
            }

            //Refresh Pieces
            _otherPlayerPanel1._settlementsCountText.text = player.PieceManager.GetPiecesOnBoard<Settlement>( ).Count.ToString( );
            _otherPlayerPanel1._citiesCountText.text = player.PieceManager.GetPiecesOnBoard<City>( ).Count.ToString( );
            //_otherPlayerPanel1._metropolisCountText.text = player.PieceManager.GetPiecesOnBoard<Metropolis>( ).Count.ToString( );
            _otherPlayerPanel1._basicKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<BasicKnight>( ).Count.ToString( );
            _otherPlayerPanel1._strongKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<StrongKnight>( ).Count.ToString( );
            _otherPlayerPanel1._mightyKnightsCountText.text = player.PieceManager.GetPiecesOnBoard<MightyKnight>( ).Count.ToString( );
            _otherPlayerPanel1._roadsCountText.text = player.PieceManager.GetPiecesOnBoard<Road>( ).Count.ToString( );
            _otherPlayerPanel1._shipsCountText.text = player.PieceManager.GetPiecesOnBoard<Ship>( ).Count.ToString( );
            _otherPlayerPanel1._handSizeText.text = handSize.ToString( );
            _otherPlayerPanel1._progressCardsCountText.text = progressCardCount.ToString( );

            //TODO (eric): Get or calculate victory points
        }
    }

    public void buildShip( )
    {
        Ship ship = GameController.GetLocalPlayer( ).PieceManager.GetFreePiece<Ship>( );

        if( ship != null )
        {
            GameController.CreateBuildInteractables( ship );
        }
        else
        {
            DebugConsole.Log( "You do not have any free ship pieces." );
        }
    }

    public void buildRoad( )
    {
        Road road = GameController.GetLocalPlayer( ).PieceManager.GetFreePiece<Road>( );

        if( road != null )
        {
            GameController.CreateBuildInteractables( road );
        }
        else
        {
            DebugConsole.Log( "You do not have any free road pieces." );
        }

    }

    public void upgradeSettlement( )
    {
        City city = GameController.GetLocalPlayer( ).PieceManager.GetFreePiece<City>( );
        if( city != null )
        {
            GameController.CreateBuildInteractables( city );
        }
        else
        {
            DebugConsole.Log( "You do not have any free city pieces." );
        }
    }

    public void buildSettlement( )
    {
        Settlement settlement = GameController.GetLocalPlayer( ).PieceManager.GetFreePiece<Settlement>( );
        if( settlement != null )
        {
            GameController.CreateBuildInteractables( settlement );
        }
        else
        {
            DebugConsole.Log( "You do not have any free settlement pieces." );
        }
    }

    public void moveShip( )
    {
        GameController.CreateMoveSelectionInteractables<Ship>( );
    }

	public void buildKnight() {
		BasicKnight knight = GameController.GetLocalPlayer ().PieceManager.GetFreePiece<BasicKnight> ();
		if (knight != null) {
			GameController.CreateBuildInteractables (knight);
		}
		else {
			DebugConsole.Log ("You do not have any free basic knight pieces.");
		}
	}

	public void moveKnight() {
		GameController.CreateMoveSelectionInteractables<Knight> ();
	}

	public void promoteKnight() {
		StrongKnight strongKnight = GameController.GetLocalPlayer ().PieceManager.GetFreePiece<StrongKnight> ();
		MightyKnight mightyKnight = GameController.GetLocalPlayer ().PieceManager.GetFreePiece<MightyKnight> ();

		if (strongKnight != null) {
			GameController.CreateBuildInteractables (strongKnight);
		} 
		else {
			DebugConsole.Log ("You do not have any free strong knight pieces.");
		}

		if (mightyKnight != null) {
			GameController.CreateBuildInteractables (mightyKnight);
		} 
		else {
			Debug.Log ("You do not have any free mighty knight pieces.");
		}
	}

	public void activateKnight() {
		GameController.CreateActivateKnightInteractables ();
	}

	public void drawTradeCard() {
		ProgressCard progressCard = GameController.GetRandomAvailableTradeCard();
		GameController.GetLocalPlayer ().CmdAddCards (new string[] { progressCard.type.ToString () });

		GameController.CmdReceiveReply ();
		_chooseProgressCardPanel.SetActive (false);
	}

	public void drawPoliticsCard() {
		ProgressCard progressCard = GameController.GetRandomAvailablePoliticsCard();
		GameController.GetLocalPlayer ().CmdAddCards (new string[] { progressCard.type.ToString () });

		GameController.CmdReceiveReply ();
		_chooseProgressCardPanel.SetActive (false);
	}

	public void drawScienceCard() {
		ProgressCard progressCard = GameController.GetRandomAvailableScienceCard();
		GameController.GetLocalPlayer ().CmdAddCards (new string[] { progressCard.type.ToString () });

		GameController.CmdReceiveReply ();
		_chooseProgressCardPanel.SetActive (false);
	}

    public void endTurnGC( )
    {
        _endTurnButton.gameObject.SetActive( false );
        _actionButton.gameObject.SetActive( false );
        _actionPanel.gameObject.SetActive( false );
        GameController.GetLocalPlayer( ).CmdEndTurn( );
    }

    public void rollDiceGC( )
    {
        _actionButton.gameObject.SetActive( true );
        _actionPanel.gameObject.SetActive( false );
        GameController.GetLocalPlayer( ).CmdRollDice( );
        _endTurnButton.gameObject.SetActive( true );
        _rollDiceButton.gameObject.SetActive( false );
    }

    public void OnClickBankTradeActionButton( )
    {
        ToggleGameObject( _bankTradeWindow );
    }

    public void BankTrade( )
    {

        int offer, request;
        Dropdown[] tradeDropdowns;

        tradeDropdowns = this.GetComponentsInChildren<Dropdown>( true );
        //Dropdown dropdownOffer = Globals.GAME_CANVAS.transform.Find("");
        offer = tradeDropdowns[0].value;
        request = tradeDropdowns[1].value;

        DebugConsole.Log( offer.ToString( ) );
        DebugConsole.Log( request.ToString( ) );
        GameController.GetLocalPlayer( ).CmdStartTrade( offer, request );
    }

    public void OnClickSaveGameButton( )
    {
        if( _saveFileInput.text == "" )
        {
            DebugConsole.Log( "Please input a save file name", "warning" );
            return;
        }

        GameController.SaveGame( _saveFileInput.text );
        _mainMenuPanel.SetActive( false );
    }

    public void OnClickQuitGameButton( )
    {
        Application.Quit( );
    }

    public void ToggleMainMenu( )
    {
        ToggleGameObject( _mainMenuPanel );
    }

    public void ToggleActionPanel( )
    {
        ToggleGameObject( _actionPanel.gameObject );
    }

    public void ToggleOtherPlayerPanels( )
    {
        ToggleGameObject( _otherPlayerPanels.gameObject );
    }

    public void ToggleLocalPlayerPanel( )
    {
        ToggleGameObject( _localPlayerPanel.gameObject );
    }

    public void ToggleGameObject( GameObject objectToToggle )
    {
        if( objectToToggle.activeInHierarchy )
        {
            objectToToggle.SetActive( false );
        }
        else
        {
            objectToToggle.SetActive( true );
        }
    }
}
