﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreMatchUI : MonoBehaviour
{
    public Button _refreshMatchListButton;
    public MatchList _matchList;
    public InputField _matchNameInput;
    public Button _createNewMatchButton;
    public SimpleObjectPool _matchButtonPool;
    public CatanNetworkManager _networkManager;

	// Use this for initialization
	void Start ()
    {
        _networkManager.StartMatchMaker( );
    }

    public void CreateMatch( )
    {
        //Disable the pregame UI, so that the GameController phases can take over.
        gameObject.SetActive( false );

        if( _matchNameInput.text == "" )
        {
            _matchNameInput.text = "DefaultMatchName";
        }

        _networkManager.matchName = _matchNameInput.text;
        _networkManager.matchMaker.CreateMatch( _networkManager.matchName, _networkManager.matchSize, true, "", "", "", 0, 0, _networkManager.OnMatchCreate );
    }
}
