﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileList : MonoBehaviour
{
    public CatanNetworkManager networkManager;
    public SimpleObjectPool fileButtonPool;
    public Transform contentPanel;

    public void Update( )
    {
        //This involves network calls and probably shouldn't be called every frame.
        //But it will work if it is here.
    }

    public void RefreshList( )
    {
        RemoveButtons( );
        AddButtons( );
    }

    private void RemoveButtons( )
    {
        while( contentPanel.transform.childCount > 0 )
        {
            GameObject toRemove = transform.GetChild( 0 ).gameObject;
            fileButtonPool.ReturnObject( toRemove );
        }
    }

    private void AddButtons( )
    {
        DirectoryInfo directory = new DirectoryInfo( Application.persistentDataPath );
        FileInfo[] files = directory.GetFiles( "*.dat" );

        foreach( FileInfo file in files )
        {
            GameObject newFileButton = fileButtonPool.GetObject( );
            newFileButton.transform.SetParent( contentPanel.transform );
            newFileButton.GetComponent<FileButton>( ).Setup( file );
        }
    }
}
