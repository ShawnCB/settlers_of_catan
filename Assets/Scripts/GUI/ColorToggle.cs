﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorToggle : MonoBehaviour
{
    public int colorId;
    public bool isToggled = false;
    private GameController _gameController = null;

    public void OnToggle( )
    {
        if( _gameController == null )
        {
            _gameController = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );

            if( _gameController == null )
            {
                DebugConsole.Log( "Could not find GameController object", "error" );
                return;
            }
        }

        isToggled = !isToggled;

        if( isToggled )
        {
            _gameController.GetLocalPlayer( ).CmdSetPlayerColor( colorId );
        }
        else
        {
            _gameController.GetLocalPlayer( ).CmdSetPlayerColor( -1 );
        }
    }
}
