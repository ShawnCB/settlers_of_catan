﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalPlayerPanel : MonoBehaviour
{
    public Text _settlementsCountText;
    public Text _citiesCountText;
    public Text _metropolisCountText;
    public Text _basicKnightsCountText;
    public Text _strongKnightsCountText;
    public Text _mightyKnightsCountText;
    public Text _roadsCountText;
    public Text _shipsCountText;
}
