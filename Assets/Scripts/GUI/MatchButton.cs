﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking.Match;

public class MatchButton : MonoBehaviour
{
    public Button _button;
    public Text _matchNameText;
    public Text _matchPlayersText;

    private CatanNetworkManager _networkManager;
    private MatchInfoSnapshot _matchInfo;
    private UIManager _uiManager;

	// Use this for initialization
	void Start ()
    {
        _uiManager = GameObject.Find( "UIManager" ).GetComponent<UIManager>( );
        _networkManager = GameObject.Find( "CatanNetworkManager" ).GetComponent<CatanNetworkManager>( );
        _button.onClick.AddListener( OnClick );
	}

    public void Setup( MatchInfoSnapshot matchInfoSnapshot )
    {
        _matchInfo = matchInfoSnapshot;
        _matchNameText.text = matchInfoSnapshot.name;
        _matchPlayersText.text = matchInfoSnapshot.currentSize + "/" + matchInfoSnapshot.maxSize;
    }

    public void OnClick( )
    {
        //Disable the pregame UI, so that the GameController phases can take over.
        _uiManager._preMatchUI.gameObject.SetActive( false );
        _networkManager.matchName = _matchInfo.name;
        _networkManager.matchSize = (uint)_matchInfo.currentSize;
        _networkManager.matchMaker.JoinMatch( _matchInfo.networkId, "", "", "", 0, 0, _networkManager.OnMatchJoined );
    }
}
