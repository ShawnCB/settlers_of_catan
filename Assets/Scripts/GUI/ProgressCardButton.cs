﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressCardButton : MonoBehaviour
{
    public Button _button;
    public Text _buttonText;
    public Text _cardCountText;

    private ProgressCard _progressCardType;

    // reference to the game
    private GameController _gameController = null;
    private GameController GameController
    {
        get
        {
            //Note: We used to find the GameController object in
            //      PlayerController::Start. However, clients can create
            //      PlayerController objects before the GameController object
            //      has been created, causing errors. Therefore, we simply
            //      delay retrieval of the GameController object until it is
            //      first required.
            //      
            //      This can potentially return null if called before the
            //      GameController has been constructed, so take care in when
            //      you ask for it.
            if( _gameController == null )
            {
                //Expects us to only ever have at most one GameController in the scene.
                GameObject gameControllerObj = GameObject.FindWithTag( "GameController" );
                if( gameControllerObj != null )
                {
                    _gameController = gameControllerObj.GetComponent<GameController>( );
                }
            }
            return _gameController;
        }
        set
        {
            _gameController = value;
        }
    }

    // Use this for initialization
    void Start( )
    {
        _button.onClick.AddListener( OnClick );
    }

    public void Setup( ProgressCard type, int count )
    {
        _progressCardType = type;
        _buttonText.text = type.type.ToString( );
    }

    public void OnClick( )
    {
        _progressCardType.PlayCard( );
        GameController.GetLocalPlayer( ).CmdRemoveCards( new string[] { _progressCardType.type.ToString( ) } );
    }
}
