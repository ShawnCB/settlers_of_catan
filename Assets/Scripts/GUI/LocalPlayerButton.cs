﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalPlayerButton : MonoBehaviour
{
    public Text _oreAmountText;
    public Text _woolAmountText;
    public Text _brickAmountText;
    public Text _grainAmountText;
    public Text _lumberAmountText;
    public Text _goldAmountText;
    public Text _clothAmountText;
    public Text _coinAmountText;
    public Text _paperAmountText;
    public Text _progressCardAmountText;
    public Text _victoryPointAmountText;
}
