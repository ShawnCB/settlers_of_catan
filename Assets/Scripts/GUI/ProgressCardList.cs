﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressCardList : MonoBehaviour
{
    public SimpleObjectPool progressCardButtonPool;
    public Transform contentPanel;

    // reference to the game
    private GameController _gameController = null;
    private GameController GameController
    {
        get
        {
            //Note: We used to find the GameController object in
            //      PlayerController::Start. However, clients can create
            //      PlayerController objects before the GameController object
            //      has been created, causing errors. Therefore, we simply
            //      delay retrieval of the GameController object until it is
            //      first required.
            //      
            //      This can potentially return null if called before the
            //      GameController has been constructed, so take care in when
            //      you ask for it.
            if( _gameController == null )
            {
                //Expects us to only ever have at most one GameController in the scene.
                GameObject gameControllerObj = GameObject.FindWithTag( "GameController" );
                if( gameControllerObj != null )
                {
                    _gameController = gameControllerObj.GetComponent<GameController>( );
                }
            }
            return _gameController;
        }
        set
        {
            _gameController = value;
        }
    }

    public void Update( )
    {
        //This involves network calls and probably shouldn't be called every frame.
        //But it will work if it is here.
    }

    public void RefreshList( )
    {
        RemoveButtons( );
        AddButtons( );
    }

    private void RemoveButtons( )
    {
        while( contentPanel.transform.childCount > 0 )
        {
            GameObject toRemove = transform.GetChild( 0 ).gameObject;
            progressCardButtonPool.ReturnObject( toRemove );
        }
    }

    private void AddButtons( )
    {
        Dictionary<ProgressCard,int> progressCards = GameController.GetLocalPlayer( ).ProgressCards;

        foreach( KeyValuePair<ProgressCard, int> entry in progressCards )
        {
            GameObject newFileButton = progressCardButtonPool.GetObject( );
            newFileButton.transform.SetParent( contentPanel.transform );
            newFileButton.GetComponent<ProgressCardButton>( ).Setup( entry.Key, entry.Value );
        }
    }
}
