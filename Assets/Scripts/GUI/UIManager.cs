﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public PreMatchUI _preMatchUI;
    public LobbyPhaseUI _lobbyPhaseUI;
    public MainPhaseUI _mainPhaseUI;

    public void RegisterPlayers( List<PlayerController> players )
    {
        int otherPlayerCount = 0;

        foreach( PlayerController player in players )
        {
            if( !player.isLocalPlayer )
            {
                otherPlayerCount++;
                _mainPhaseUI.RegisterPlayer( player, otherPlayerCount );
            }
            else
            {
                _mainPhaseUI.RegisterPlayer( player, 0 );
            }
        }
    }

    // Use this for initialization
    void Start ()
    {
        _mainPhaseUI.gameObject.SetActive(false);
	}
}
