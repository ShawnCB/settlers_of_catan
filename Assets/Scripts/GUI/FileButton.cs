﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileButton : MonoBehaviour
{
    public Button _button;
    public Text _buttonText;
    
    private FileInfo _file;

    // Use this for initialization
    void Start( )
    {
        _button.onClick.AddListener( OnClick );
    }

    public void Setup( FileInfo file )
    {
        _file = file;
        _buttonText.text = file.Name;
    }

    public void OnClick( )
    {
        GameController gameController = GameObject.FindWithTag( "GameController" ).GetComponent<GameController>( );

        if( gameController == null )
        {
            DebugConsole.Log( "Could not find GameController object", "error" );
        }

        gameController.LoadGame( _file.Name );

        //Hide the load game list.
        LobbyPhaseUI lobbyUI = GameObject.Find( "UIManager/LobbyPhaseUI" ).GetComponent<LobbyPhaseUI>( );
        lobbyUI._fileList.gameObject.SetActive( false );
    }
}
